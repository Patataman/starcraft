package org.iaie.tutorial2;

import jnibwapi.BWAPIEventListener;
import jnibwapi.JNIBWAPI;
import jnibwapi.Position;
import jnibwapi.Position.PosType;
import jnibwapi.Unit;
import jnibwapi.types.TechType;
import jnibwapi.types.TechType.TechTypes;
import jnibwapi.types.UnitType;
import jnibwapi.types.UnitType.UnitTypes;
import jnibwapi.types.UpgradeType;
import jnibwapi.types.UpgradeType.UpgradeTypes;

import java.awt.Point;
import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.iaie.Agent;
import org.iaie.tools.Options;


public class PlayerTutorial203168900315536 extends Agent implements BWAPIEventListener {

	/** Esta variable se usa para almacenar aquellos depositos de minerales 
     *  que han sido seleccionados para ser explotados por las unidades 
     *  recolectoras. */
    //private final HashSet<Unit> claimedMinerals = new HashSet<>();
	//SE IGNORA PORQUE YA SE CONTROLAN LOS TRABAJADORES EN LOS MINERALES AL CREARLOS

    /** Esta variable se utiliza la generaci�n de VCEs */
    private boolean scvReady;

    /** Esta variable se utiliza como contenedor temporal para selecionar
     *  un vce que va a ponerse a trabajar.*/
    private Unit scvWorking;
    private Unit commandCenter;

    /** Esta variable se utiliza para comprobar cuando debe se debe construir
     * un nuevo dep�sito de suministros.*/
    private int supplyCap;
    
    private byte VCEs;
   
    private byte barracones;
    private int[][] mapa;

    public PlayerTutorial203168900315536() {            

        // Generaci�n del objeto de tipo agente

        // Creaci�n de la superclase Agent de la que extiende el agente, en este m�todo se cargan            
        // ciertas variables de de control referentes a los par�metros que han sido introducidos 
        // por teclado. 
        super();
        // Creaci�n de una instancia del connector JNIBWAPI. Esta instancia s�lo puede ser creada
        // una vez ya que ha sido desarrollada mediante la utilizaci�n del patr�n de dise�o singlenton.
        this.bwapi = new JNIBWAPI(this, true);
        // Inicia la conexi�n en modo cliente con el servidor BWAPI que est� conectado directamente al videojuego.
        // Este proceso crea una conexi�n mediante el uso de socket TCP con el servidor. 
        this.bwapi.start();
    }

    /**
     * Este evento se ejecuta una vez que la conexi�n con BWAPI se ha estabilidad. 
     */
    @Override
    public void connected() {
        System.out.println("IAIE: Conectando con BWAPI");
    }

    /**
     * Este evento se ejecuta al inicio del juego una �nica vez, en el se definen ciertas propiedades
     * que han sido leidas como par�metros de entrada.
     * Velocidad del juego (Game Speed): Determina la velocidad a la que se ejecuta el videojuego. Cuando el juego 
     * se ejecuta a m�xima velocidad algunas eventos pueden ser detectados posteriormente a su ejecuci�n real. Esto
     * es debido a los retrasos en las comunicacion y el retardo producido por la tiempo de ejecuci�n del agente. En 
     * caso de no introducir ningun valor el jugador 
     * Informaci�n perfecta (Perfect informaci�n): Determina si el agente puede recibir informaci�n completa del 
     * juego. Se consedira como informaci�n perfecta cuando un jugador tiene acceso a toda la informaci�n del entorno, 
     * es decir no le afecta la niebla de guerra.
     * Entrada de usuarios (UserInput)
     */
    @Override
    public void matchStart() {

        System.out.println("IAIE: Iniciando juego");

        // Revisar. 
        // Mediante est� metodo se puede obtener informaci�n del usuario. 
        if (Options.getInstance().getUserInput()) this.bwapi.enableUserInput();
        // Mediante este m�todo se activa la recepci�n completa de informaci�n.
        if (Options.getInstance().getInformation()) this.bwapi.enablePerfectInformation();
        // Mediante este m�todo se define la velocidad de ejecuci�n del videojuego. 
        // Los valores posibles van desde 0 (velocidad est�ndar) a 10 (velocidad m�xima).
        this.bwapi.setGameSpeed(Options.getInstance().getSpeed());

        // Iniciamos las variables de control
        // Se establece el contador de objetos a cero y se eliminan todas las
        // referencias previas a los objetos anteriormente a�adidos.
        //claimedMinerals.clear();
        // Se establecen los valores por defecto de las variables de control.
        scvReady = false;
        scvWorking = null;
        supplyCap = 0;
        VCEs = 4;
        barracones = 0;
        
    	for (Unit cc : this.bwapi.getMyUnits()){
			if (cc.getType() == UnitTypes.Terran_Command_Center) {
				commandCenter = cc;
			}	
		}
    	
    	//Se crea la representaci�n del mapa
    	createMap();
    }

    /**
     * Evento Maestro
     */
    @Override
    public void matchFrame() {

        String msg = "=";
        // Mediante este bucle se comprueba si el jugador est� investigando
        // alg�n tipo de tecnolog�a y lo muestra por pantalla
        for (TechType t : TechTypes.getAllTechTypes()) {
            if (this.bwapi.getSelf().isResearching(t)) {
                msg += "Investigando " + t.getName() + "=";
            }
            // Exclude tech that is given at the start of the game
            UnitType whatResearches = t.getWhatResearches();
            if (whatResearches == UnitTypes.None) {
                continue;
            }
            if (this.bwapi.getSelf().isResearched(t)) {
                msg += "Investigado " + t.getName() + "=";
            }
        }

        // Mediante este bucle se comprueba si se est� realizando una actualizaci�n
        // sobre alg�n tipo de unidad. 
        for (UpgradeType t : UpgradeTypes.getAllUpgradeTypes()) {
            if (this.bwapi.getSelf().isUpgrading(t)) {
                msg += "Actualizando " + t.getName() + "=";
            }
            if (this.bwapi.getSelf().getUpgradeLevel(t) > 0) {
                int level = this.bwapi.getSelf().getUpgradeLevel(t);
                msg += "Actualizado " + t.getName() + " a nivel " + level + "=";
            }
        }
        
        //Bucle para averiguar cuantos barracones se tienen.
        //Algo guarro, pero es lo que hay.
        barracones = 0;
        for (Unit building : this.bwapi.getMyUnits()){
        	if (building.getType() == UnitTypes.Terran_Barracks){
        		barracones += 1;
        	}
        }

        this.bwapi.drawText(new Position(0, 20), msg, true);

        // Mediante este m�todo se 
        this.bwapi.getMap().drawTerrainData(bwapi);

        if (scvWorking != null && (scvWorking.isIdle() || scvWorking.isGatheringMinerals()
        							|| scvWorking.isGatheringGas())){
        	scvWorking = null;
        }

        // Proceso para crear un vce.
        // Lo ideal son 3 recolectores por nodo de recursos.
        // Dado que todos los mapas empiezan con los mismos
        // nodos de minerales, se producen VCEs hasta que se llegue al l�mite.
        for (Unit unit : this.bwapi.getMyUnits()) {
            // Se comprueba si el n�mero de minerales es superios a 50 unidades
            // y si no se han creado el n�mero m�ximo de VCE.
        	//Se recupera el centro de mando y se le pone a fabricar un VCE.
            if (this.bwapi.getSelf().getMinerals() >= 50 && !scvReady
        		&& unit.getType() == UnitTypes.Terran_Command_Center) {                            
        			//Si el centro de mando est� completado y no est� entrenando unidades
        			if (unit.isCompleted() && !unit.isTraining()){
        				//Se a�ade un VCE a la cola
        				if (trainUnit(unit.getID(), UnitTypes.Terran_SCV)) {
        					VCEs++;
        					if (VCEs == 24){
        						scvReady = true;                		
        					}        					
        				}
        			}
    		}
        }

        // Proceso para la recolecci�n de minerales
        for (Unit unit : this.bwapi.getMyUnits()) {
            // Se comprueba para cada unidad del jugador que est� siendo 
            // controlado si este de tipo VCE
            if (unit.getType() == UnitTypes.Terran_SCV) {
                // Se comprueba si la unidad no est� realizando ninguna tarea (isIdle)
                if ((unit.isIdle() || (unit.isIdle() && unit.isCarryingMinerals()) && !unit.equals(scvWorking))) {
                    // Se comprueban para todas las unidades de tipo neutral, aquella
                    // que no pertenencen a ningun jugador. 
                    for (Unit recurso : this.bwapi.getNeutralUnits()) {
                        // Se comprueba si la unidad es un deposito de minerales y si es
                        // no ha sido seleccionada previamente.                                 
                        if (recurso.getType().isMineralField()) {                                    
                            // Se calcula la distancia entre la unidad y el deposito de minerales
                            double distance = unit.getDistance(recurso);                                    
                            // Se comprueba si la distancia entre la unidad 
                            // y el deposito de minerales es menor a 300.
                            if (distance < 300) {
                                // Se ejecuta el comando para enviar a la unidad a recolertar
                                // minerales del deposito seleccionado.
                                unit.rightClick(recurso, false);
                                // Se a�ade el deposito a la lista de depositos en uso.
                                //this.claimedMinerals.add(minerals);                                	
                                break;
                            }
                        }
                    }
                    
                }
            }
        }
        
        // Proceso de construcci�n de dep�sitos de suministros.
        // Similar al proceso de construir barracones.
        // Se comprueba si el n�mero de unidades generadas (getSupplyUsed) es mayor que el
        // n�mero de unidades disponibles (getSupplyTotal) y si el n�mero de unidades 
        // disponibles es mayor que el valor de la variable supplyCap. 
        if (bwapi.getSelf().getSupplyUsed() + 2 >= bwapi.getSelf().getSupplyTotal()
        		&& bwapi.getSelf().getSupplyTotal() > supplyCap) {
        	// Se comprueba si el n�mero de minerales disponibles es 
        	// mayor o igual a 100.
        	if (bwapi.getSelf().getMinerals() >= 100) {
        		for (Unit vce : bwapi.getMyUnits()) {
        			// Se comprueba si la unidades es de tipo VCE.
        			if (vce.getType() == UnitTypes.Terran_SCV && (vce.isIdle() || vce.isGatheringMinerals()) && scvWorking == null ) {
        				Position pos = commandCenter.getPosition();
        				//Se construye un dep�sito de suministros
        				scvWorking = vce;
        				buildUnit(vce.getID(), UnitTypes.Terran_Supply_Depot, pos);
        				// Se asigna un nuevo valor a la variable supplyCap
        				supplyCap = bwapi.getSelf().getSupplyTotal();
        			}
        		}
        	}
        }

        // Proceso de contrucci�n de unos barracones (Para entrenar marines)
        // Se comprueba si el n�mero de minerales disponibles es superior a 150
        // y no se ha construido otros barracones.
        if (this.bwapi.getSelf().getMinerals() >= 150 && barracones==0){
            //Construcci�n de los barracones cerca del Centro de Mando.
            //Hay que localizar el centro de mando y encontrar una posici�n cercana
            //Que permita construir unos barracones + 1 bloque extra para que las unidades puedan moverse.
            for (Unit vce : this.bwapi.getMyUnits()) {
            	// Se encuentra un VCE para construir
            	if (vce.getType() == UnitTypes.Terran_SCV && scvWorking==null) {
            		Position pos = commandCenter.getPosition();
    				scvWorking = vce;
    				// Se construye el barrac�n en la posici�n obtenida
    				buildUnit(vce.getID(), UnitTypes.Terran_Barracks, pos);
            	}
            }
        }

        // Proceso de generaci�n de unidades, es necesario disponer de suministros
        // Se comprueba si el n�mero de minerales disponibles es 
        // mayor o igual a 50.
        else{
        	//Generar soldados
        	if (bwapi.getSelf().getMinerals() >= 50) {
        		for (Unit unit : bwapi.getMyUnits()) {
        			// Se compruba si existe un Barrac�n y si est� ha sido completada
        			if (unit.getType() == UnitTypes.Terran_Barracks && unit.isCompleted() && 
        					unit.getTrainingQueueSize() <= 2) {
        				trainUnit(unit.getID(), UnitTypes.Terran_Marine);
        			}
        		}
        	}        	
        }
        
        //Construcci�n de la refiner�a.
        //Deben construirse barracones previamente, porque de cara al futuro
        //dado que para producir soldados no se necesita vespeno, es prioritario
        //sacar los barracones antes que hacer la refiner�a.
        if (barracones != 0 && 
        	scvWorking==null && this.bwapi.getSelf().getMinerals()>=100){
        	//se busca 1 constructor
        	for (Unit vce : this.bwapi.getMyUnits()){
        		//Se verifica que no haga nada con su vida
        		if (vce.isIdle() || vce.isGatheringMinerals()) {
        			//Se busca el vespeno
        			for (Unit vespeno : this.bwapi.getNeutralUnits()){
        				if (vespeno.getType() == UnitTypes.Resource_Vespene_Geyser){
        					//Se obtiene la pos. del vespeno.
        					Position pos = vespeno.getTilePosition();
        					scvWorking = vce;
        					buildUnit(vce.getID(), UnitTypes.Terran_Refinery, pos);
        					//Refinerias=1 -> Ya se ha construido la refiner�a
        					break;        					
        				}
        			}        
        			break;
        		}
        		
        	}
        }
    }

    @Override
    public void keyPressed(int keyCode) {}
    @Override
    public void matchEnd(boolean winner) {}	
    @Override
    public void sendText(String text) {}
    @Override
    public void receiveText(String text) {}
    @Override
    public void nukeDetect(Position p) {}
    @Override
    public void nukeDetect() {}
    @Override
    public void playerLeft(int playerID) {}
    @Override
    public void unitCreate(int unitID) {}
    @Override
    public void unitDestroy(int unitID) {}
    @Override
    public void unitDiscover(int unitID) {}
    @Override
    public void unitEvade(int unitID) {}
    @Override
    public void unitHide(int unitID) {}
    @Override
    public void unitMorph(int unitID) {}
    @Override
    public void unitShow(int unitID) {}
    @Override
    public void unitRenegade(int unitID) {}
    @Override
    public void saveGame(String gameName) {}
    @Override
    public void unitComplete(int unitID) { }
    @Override
    public void playerDropped(int playerID) {}
    
    /*
     * M�todo para entrenar una unidad en un edificio.
     * Si la unidad no se puede entrenar, devuelve False.
     * En caso contrario, True.
     */
    public boolean trainUnit(int buildingId, UnitType unitId){
    	if (bwapi.getSelf().getSupplyUsed() + unitId.getSupplyRequired() <= bwapi.getSelf().getSupplyTotal()){
    		//Se recupera el edificio
    		Unit building = bwapi.getUnit(buildingId);
    		building.train(unitId);
    		return true;
    	}
    	return false;
    }
    
    /*
     * Dada una posici�n, devuelve una posici�n
     * v�lida para construir un edificio.
     */
    public Position getValidPos(Position pos, UnitType building){
    	//Verifica si se puede construir en la posici�n dada y si la unidad asignada puede construir.
    	//En caso afirmativo devuelve True, en caso contrario False.
    	byte dirX = 1, dirY = 1;
    	if (Math.random() < 0.5){
    		dirX = -1;
    	}
    	if (Math.random() > 0.5){
    		dirY = -1;
    	}
    	while (!this.bwapi.canBuildHere(pos, building, false)){
    		Position s = new Position(dirX*building.getTileWidth(), dirY*building.getTileHeight());
    		pos = pos.translated(s);
    	}
    	return pos;
    }
    
    /*
     * M�todo para construir un edificio.
     * Para poder construir un edificio en X Pos, el VCE debe
     * moverse a esa posici�n antes de empezar a construir
     */
    public void buildUnit(int workerId, UnitType buildingId, Position pos){
    	if(buildingId.getMineralPrice() <= bwapi.getSelf().getMinerals() &&
    			buildingId.getGasPrice() <= bwapi.getSelf().getGas()) {
    		//Si no es refiner�a (porque la posici�n es fija)
    		//Se busca una posici�n de construcci�n v�lida
    		if (buildingId != UnitTypes.Terran_Refinery){
    			pos = this.getValidPos(pos, buildingId);    		
    		}

    		//Se recupera el trabajador
    		Unit worker = bwapi.getUnit(workerId);
    		
    		worker.build(pos, buildingId);
    	}
    }
    
    /*
     * M�todo que genera un mapa con los tama�os m�ximos
     * de edificios que se pueden construir desde una casilla
     * hacia abajo a la derecha.
     * 
     * V -> Vespeno
     * M -> Minerales
     * 0 -> No se puede construir
     * 
     * Se genera una posici�n inicial (0,0) y se va recorriendo
     * todo el mapa mirando si es construible/caminable una posici�n
     * 
     * Una vez se tiene generado el mapa se miran todos los recursos
     * y se sit�an en el mapa.
     */
    public void createMap() {
    	//La posici�n hay que desplazarla de 32 en 32 (tama�o de las casillas)
    	Position pos_aux = new Position(0,0, PosType.BUILD);
    	//Altura m�xima del mapa en pixeles
		int maxHeight = this.bwapi.getMap().getSize().getBY();
		//Anchura m�xima del mapa en pixeles
		int maxWidth = this.bwapi.getMap().getSize().getBX();
		//Altura de la casilla actual
		int altura = this.bwapi.getMap().getGroundHeight(pos_aux);
		//Mapa a devolver
		mapa = new int[maxHeight][maxWidth];
		//Tama�o m�ximo del edificio que se puede construir
		int dimension;
		//Variable que detiene la b�squeda si se encuentra un obst�culo
		boolean zonaLibre;
		//Indica si la siguiente posici�n es un cambio de altura
		
		// new Position(c,f, PosType.BUILD) 
		for(int f = 0; f < maxHeight; f++){
			for(int c = 0; c < maxWidth; c++){
				pos_aux = new Position(c, f, PosType.BUILD);
				dimension = 0;
				zonaLibre = true;
				
				//para cada posici�n se mira si se est� en los l�mites del mapa y
				//hay que verificar si la posici�n es construible
				if (f+1>=maxHeight || c+1>=maxWidth){
					zonaLibre = false;
				}
				if (!this.bwapi.getMap().isBuildable(pos_aux)){
					zonaLibre = false;
				}
				//Se obtiene la altura de la posici�n
				altura = this.bwapi.getMap().getGroundHeight(pos_aux);
				while(zonaLibre && dimension <= 4){
					dimension++;
					//Se verifica vertical, horizontal y diagonalmente si son v�lidas las posiciones.
					//Si alguna no lo es, se sale del while y se guarda el valor en el mapa
					for(int i = 0; i < dimension; i++){
						//matriz[i+f][c+dimension]	Comprueba columnas
						if (bwapi.isBuildable(new Position(c+dimension, f+i, PosType.BUILD), true)){ // �Es construible?
							if(bwapi.getMap().getGroundHeight(new Position(c+dimension, f+i, PosType.BUILD)) != altura){ // �Est�n a diferente altura?
								zonaLibre = false;
							}
						}
						else{
							zonaLibre = false;
						}
						
						//matriz[f+dimension][i+c]    Comprueba filas
						if (bwapi.isBuildable(new Position(c+i, f+dimension, PosType.BUILD), true)) {
							if(bwapi.getMap().getGroundHeight(new Position(c+i, f+dimension, PosType.BUILD)) != altura){
								zonaLibre = false;
							}
						}
						else{
							zonaLibre = false;
						}
					
						//matriz[f+dimension][c+dimension]   Comprueba en la diagonal (se podr�a cambiar cambiando la condicion del for a <=)
						if (bwapi.isBuildable(new Position(c+dimension, f+dimension, PosType.BUILD), true)) {
							if(bwapi.getMap().getGroundHeight(new Position(c+dimension, f+dimension, PosType.BUILD)) != altura){
								zonaLibre = false;
							}
						}
						else{
							zonaLibre = false;
						}
					}
					//si se est� en los l�mites del mapa en la pr�xima iteraci�n, se sale del bucle.  //creo que no hace falta porque isBuildable ya lo comprueba
					if (f+1+dimension>=maxHeight || c+1+dimension>=maxWidth){
						zonaLibre = false;
					}

				}
				// Se resta 1 porque hemos aumentado en 1 la dimensi�n suponiendo que la siguiente posici�n es v�lida
				if (dimension != 0)
					dimension--;
				// Se actualiza la posici�n
				mapa[f][c] = (dimension);	
			}
		}
		
		// Ahora se buscan los nodos de recursos y se les pone valores especiales:
		// -1 Para minerales
		// -2 Para vespeno.
		// getTilePosition devuelve la posici�n superior izquierda
		for (Unit u : bwapi.getNeutralUnits()){
			if (u.getType() == UnitTypes.Resource_Mineral_Field ||
					u.getType() == UnitTypes.Resource_Mineral_Field_Type_2 ||
					u.getType() == UnitTypes.Resource_Mineral_Field_Type_3) {
				//para recolectar minerales vale con que el vce vaya a cualquiera de sus casillas.
				mapa[u.getTilePosition().getBY()][u.getTilePosition().getBX()] = -1;
			}
			if (u.getType() == UnitTypes.Resource_Vespene_Geyser) {
				//Para construir la refiner�a nos vale la casilla arriba a la izquierda.
				mapa[u.getTilePosition().getBY()][u.getTilePosition().getBX()] = -2;
			}
		}

		String workingDirectory = System.getProperty("user.dir");
		String path = workingDirectory + File.separator + "mapa.txt";
		createANDwrite(path);
	}
    
    /**
     * Para poder crear la matriz deben ser en diagonal.
     * Para contemplar casos en el que origen y m�ximo sean en horizontal (mismo Y)
     * cuando ocurra eso, se toma Y como la Y del edificio. 
     * 
     * El m�todo devolver� un Positio que indica la casilla superior izquierda v�lida donde construir el edificio.
     * Si se devuelve -1 en X no hay posici�n v�lida.
     */
    public Position findPlace(int[][] mapa, Point origen, Point maximo, UnitType building){
    	//Si no se pasan valores correctos, se devuelve posici�n inv�lida�
    	if (origen.x < 0 || origen.y < 0 ||
    			maximo.x < 0 || maximo.y < 0) {
    		return new Position(-1,0);
    	}
    	//Limites de la submatriz
    	int xOrigen;
    	int yOrigen = origen.y;
    	int xMaximo = maximo.x;
    	int yMaximo = maximo.y;
    	//Valor a buscar de posiciones
    	int max = (building.getTileHeight() > building.getTileWidth()) ? building.getTileHeight() : building.getTileWidth();
    	//Variable de control para la b�squeda
    	boolean found = false;

    	if (origen.y == maximo.y){
    		yMaximo = building.getTileHeight();
    	}
    	
    	//se recorre el mapa entre las posiciones dadas
    	for (xOrigen = origen.x; xOrigen < xMaximo && !found; xOrigen++){
    		for (yOrigen = origen.y; yOrigen < yMaximo && !found; yOrigen++){
    			//si encuentra una posici�n v�lida sale.
    			if (mapa[yOrigen][xOrigen] >= max) {
    				found = true;
    			}
    		}
    	}
    	
    	if (found) {
    		return new Position(xOrigen, yOrigen, PosType.BUILD);
    	} else {
    		return new Position(-1,0, PosType.BUILD);
    	}
    }
    
    /**
     * Se da por supuesto que las posiciones indicadas son posiciones correctas.
     * La posici�n origen ha sido obtenida mediante el m�todo findPlace y la posici�n
     * destino ha sido calculada con el tama�o del edificio + la posici�n origen
     */
    public void updateMap(Position origen, Position destino) {
    	//se recorre la matriz entre las posiciones dadas
    	for (int i = origen.getBY(); i < destino.getBY(); i++){
    		for(int j = origen.getBX(); j < destino.getBX(); j++){
    			//se ponen como ocupadas las casillas
    			mapa[i][j] = 0;
    		}
    	}
    	/*
    	 *  Para actualizar el resto de la matriz, tendremos que explorar las casillas superiores y por la izquierda.
    	 *  Dado que tambi�n hay que tener en cuenta las diagonales, se har� de tal forma que primero se actualicen
    	 *  todas las superiores incluidas las diagonales y despu�s las de la izquierda. 
    	 */
    	
    	// Esta variable se usar� para saber si hemos terminado la actualizaci�n
    	boolean parada = true;
    	// Esta variable servir� para desplazarnos verticalmente y tambien saber que dimensi�n maxima puede tener el edificio de esa casilla
    	int iv = 1;
    	// Esta variable servir� para desplazarnos horizontalmente
    	int ih = 0;
    	
    	// Bucle de actualizaci�n vertical
    	while (parada){
    		//Si no nos salimos del mapa, el valor actual de la dimensi�n no es 4 (m�ximo)
    		if (((origen.getBY()-iv >= 0 && destino.getBX()-ih >= 0) && mapa[origen.getBY()-iv][destino.getBX()-ih] > iv) && (iv < 4)){ // Si llegamos a 4 no es necesario seguir
    			mapa[origen.getBY()-iv][destino.getBX()-ih] = iv;
    			iv++;
    		}
    		else{ // Hemos terminado con la columna, pasamos a la siguiente (hacia atr�s en el mapa)
    			if (iv == 1){
    				parada = false; // Si en la primera casilla no hay que actualizar, significa que hemos terminado.
    			}
    			else{
    				ih++;
        			iv = 1;
    			}
    		}
    	}
    	
    	ih = 1;
    	iv = 0;
    	parada = true;
    	// Bucle horizontal
    	while (parada){
    		if (((origen.getBY()-iv >= 0 && origen.getBX()-ih >= 0) && mapa[origen.getBY()+iv][origen.getBX()-ih] > ih) && (ih < 4)){ // Si llegamos a 4 no es necesario seguir
    			mapa[origen.getBY()+iv][origen.getBX()-ih] = ih;
    			ih++;
    		}
    		else{ // Hemos terminado con la fila, pasamos a la siguiente (hacia abajo en el mapa)
    			if (ih == 1 || iv == destino.getBY()){
    				parada = false; // Si en la primera casilla no hay que actualizar, significa que hemos terminado.
    			}
    			else{
    				iv++;
        			ih = 1;
    			}
    		}
    	}	
    }
	
	/**
	 * M�todo que crea un archivo nuevo,
	 * si ya exist�a lo resetea y escribe en �l
	 * @param path: ruta donde se localiza el archivo
	 * @param texto: texto a escribir
	 * @return 0 -> Correcto; 1 -> Error
	 * @throws IOException
	 */
	public int createANDwrite(String path) {
		try {
			Path p = Paths.get(path);
			Charset charset = Charset.forName("UTF-8");
			//Por defecto trae CREATE y TRUNCATE
			BufferedWriter writer = Files.newBufferedWriter(p, charset);
			for(int f = 0; f < mapa.length; f++){
				for (int c=0; c < mapa[f].length; c++){
					if (mapa[f][c] == -1){
						writer.write("M;");
					}
					else if (mapa[f][c] == -2){
						writer.write("V;");
					}
					else if (mapa[f][c] < 10){
						writer.write("0"+mapa[f][c]+";");
					} 
					else {						
						writer.write(mapa[f][c]+";");
					}
				}
				writer.write("\n");
			}
			//Importante cerrar el escritor, ya que si no, no escribe
			writer.close();
			return 0;
		} catch (IOException e) {
			System.out.println(e);
			return 1;
		}
	}
    
}
