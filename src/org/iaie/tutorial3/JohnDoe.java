package org.iaie.tutorial3;

import java.util.ArrayList;
import java.util.List;

import org.iaie.btree.util.GameHandler;

import jnibwapi.JNIBWAPI;
import jnibwapi.Unit;
import jnibwapi.types.UnitType;
import jnibwapi.types.UnitType.UnitTypes;

public class JohnDoe extends GameHandler {
	
	List<Integer> trabajadoresOcupados;
	Unit worker;

	public JohnDoe(JNIBWAPI bwapi) {
		super(bwapi);
		trabajadoresOcupados = new ArrayList<Integer>();
	}
	
	
	//Obtiene un trabajador que se encuentra libre y lo agrega a la lista de ocupados
	//Un trabajador est� libre cuando recolecta mineral o no hace nada con su vida
	public boolean getWorker() { 
		for (Unit vce : this.connector.getMyUnits()) {
			// Se comprueba si la unidades es de tipo VCE y no est� ocupada
			if (vce.getType() == UnitTypes.Terran_SCV && !trabajadoresOcupados.contains(vce.getID()) && worker == null
					&& ((vce.isIdle() && vce.isCarryingMinerals()) || vce.isIdle()) ) {
				worker = vce;
				return true;
			}
		}
		return false;
	}
	
	//Se construyen dep�sitos de suministros cuando se pueden pagar y se est� a 2 del l�mite
	public boolean buildSupplies() {
        if (this.connector.getSelf().getSupplyUsed() + 2 >= this.connector.getSelf().getSupplyTotal()
        	 && this.connector.getSelf().getMinerals() >= 100) {
        	return true;
        }
        return false;
	}
	
	//Se manda a recolectar minerales al �ltimo trabajador introducido en la lista de ocupados.
	//Ya que antes de llamar a esta funci�n se llama a getWorker
	public boolean aCurrarMina(){
		for (Unit cc : this.connector.getMyUnits()) {
			if (cc.getType() == UnitTypes.Terran_Command_Center){
				for (Unit recurso : this.connector.getNeutralUnits()) {
		            if (recurso.getType().isMineralField()) {                                    
		                double distance = cc.getDistance(recurso);                                    
		                if (distance < 300) {
		                    this.connector.getUnit(worker.getID()).rightClick(recurso, false);                	
		                    trabajadoresOcupados.add(worker.getID());
		                    worker = null;
		                    return true;
		                }
		            }
				}
	        }
		}
		return false;
	}
	
	//Igual que los minerales
	public boolean aCurrarGas(){
		for (Unit refineria : this.connector.getMyUnits()) {
			if (refineria.getType() == UnitTypes.Terran_Refinery){
                this.connector.getUnit(worker.getID()).rightClick(refineria, false);                	
                trabajadoresOcupados.add(worker.getID());
                worker = null;
                return true;	
	        }
		}
		return false;
	}
	
	//Se comprueba si se posee m�s mineral y gas que el pasado por par�metro
	public boolean checkResources(int mineral, int gas){
		if (this.connector.getSelf().getMinerals() >= mineral &&
				this.connector.getSelf().getGas() >= gas){
			return true;
		}
		return false;
	}
	
	//Se comprueba si se puede construir una unidad
	public boolean canTrain(UnitType unidad) {
		if (this.connector.canMake(unidad)) {
			return true;
		}
		return false;

	}
	
	//Entrena una unidad 
	public boolean trainUnit(UnitType unidad){
		for (Unit u : this.connector.getMyUnits()){
			if (u.getType() == UnitTypes.Terran_Command_Center){
				u.train(unidad);
			}
		}
		return false;
	}

}
