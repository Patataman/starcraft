package org.iaie.tutorial3;

import org.iaie.Agent;
import org.iaie.btree.BehavioralTree;
import org.iaie.btree.task.composite.Selector;
import org.iaie.btree.task.composite.Sequence;
import org.iaie.btree.util.GameHandler;

import jnibwapi.BWAPIEventListener;
import jnibwapi.JNIBWAPI;
import jnibwapi.Position;
import jnibwapi.Unit;
import jnibwapi.types.UnitType;
import jnibwapi.types.UnitType.UnitTypes;

public class PlayerTutorial303168900315536 extends Agent implements BWAPIEventListener {

	BehavioralTree CollectTree;
	Unit buildingTree;
	UnitType unitTypeTree;
	
	public PlayerTutorial303168900315536() {            

        // Generaci�n del objeto de tipo agente

        // Creaci�n de la superclase Agent de la que extiende el agente, en este m�todo se cargan            
        // ciertas variables de de control referentes a los par�metros que han sido introducidos 
        // por teclado. 
        super();
        // Creaci�n de una instancia del connector JNIBWAPI. Esta instancia s�lo puede ser creada
        // una vez ya que ha sido desarrollada mediante la utilizaci�n del patr�n de dise�o singlenton.
        this.bwapi = new JNIBWAPI(this, true);
        // Inicia la conexi�n en modo cliente con el servidor BWAPI que est� conectado directamente al videojuego.
        // Este proceso crea una conexi�n mediante el uso de socket TCP con el servidor. 
        this.bwapi.start();
    }
	
	@Override
	public void connected() {
	}

	@Override
	public void matchStart() {
		JohnDoe gh = new JohnDoe(bwapi);
		
		unitTypeTree = UnitTypes.Terran_SCV;
		
		Selector<GameHandler> CollectResources = new Selector<>("Minerales o Vespeno");
		CollectResources.addChild(new CollectMineral("Minerales", gh));
		CollectResources.addChild(new CollectGas("Vespeno", gh));
		
		Sequence Collect = new Sequence("Recolectar");
		Collect.addChild(new FreeWorker("Trabajador libre", gh));
		Collect.addChild(CollectResources);
		
		Sequence Train = new Sequence("Entrenar");
		Train.addChild(new CheckResources("Comprobar recursos vce", gh, UnitTypes.Terran_SCV));
		Train.addChild(new ChooseBuilding("Seleccionar CC", gh, UnitTypes.Terran_SCV));
		Train.addChild(new TrainUnit("Entrenar unidad", gh, UnitTypes.Terran_SCV));
		
		CollectTree = new BehavioralTree("Primer arbol");
		CollectTree.addChild(new Selector("MAIN SELECTOR", Collect, Train));
	}

	@Override
	public void matchFrame() {
		CollectTree.run();
	}

	@Override
	public void matchEnd(boolean winner) {
		// TODO Auto-generated method stub

	}

	@Override
	public void keyPressed(int keyCode) {
		// TODO Auto-generated method stub

	}

	@Override
	public void sendText(String text) {
		// TODO Auto-generated method stub

	}

	@Override
	public void receiveText(String text) {
		// TODO Auto-generated method stub

	}

	@Override
	public void playerLeft(int playerID) {
		// TODO Auto-generated method stub

	}

	@Override
	public void nukeDetect(Position p) {
		// TODO Auto-generated method stub

	}

	@Override
	public void nukeDetect() {
		// TODO Auto-generated method stub

	}

	@Override
	public void unitDiscover(int unitID) {
		// TODO Auto-generated method stub

	}

	@Override
	public void unitEvade(int unitID) {
		// TODO Auto-generated method stub

	}

	@Override
	public void unitShow(int unitID) {
		// TODO Auto-generated method stub

	}

	@Override
	public void unitHide(int unitID) {
		// TODO Auto-generated method stub

	}

	@Override
	public void unitCreate(int unitID) {
		// TODO Auto-generated method stub

	}

	@Override
	public void unitDestroy(int unitID) {
		// TODO Auto-generated method stub

	}

	@Override
	public void unitMorph(int unitID) {
		// TODO Auto-generated method stub

	}

	@Override
	public void unitRenegade(int unitID) {
		// TODO Auto-generated method stub

	}

	@Override
	public void saveGame(String gameName) {
		// TODO Auto-generated method stub

	}

	@Override
	public void unitComplete(int unitID) {
		// TODO Auto-generated method stub

	}

	@Override
	public void playerDropped(int playerID) {
		// TODO Auto-generated method stub

	}

}
