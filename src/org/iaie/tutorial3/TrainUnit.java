package org.iaie.tutorial3;

import org.iaie.btree.state.State;
import org.iaie.btree.task.leaf.Action;
import org.iaie.btree.util.GameHandler;

import jnibwapi.types.UnitType;

public class TrainUnit extends Action {
	
	UnitType unit;
	
	public TrainUnit(String name, GameHandler gh, UnitType unit) {
		super(name, gh);
		this.unit = unit;
	}

	@Override
	public State execute() {
		try{
			if (((JohnDoe)this.handler).trainUnit(unit)) {
				return State.SUCCESS;
			} else {
				return State.FAILURE;
			}
		} catch (Exception e) {
			return State.ERROR;
		}
	}

}
