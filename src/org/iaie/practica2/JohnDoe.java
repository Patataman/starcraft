package org.iaie.practica2;

import java.awt.Point;
import java.util.ArrayList;
import java.util.List;

import org.iaie.btree.util.GameHandler;

import jnibwapi.BaseLocation;
import jnibwapi.ChokePoint;
import jnibwapi.JNIBWAPI;
import jnibwapi.Position;
import jnibwapi.Unit;
import jnibwapi.Position.PosType;
import jnibwapi.Region;
import jnibwapi.types.UnitType;
import jnibwapi.types.UnitType.UnitTypes;
import jnibwapi.types.UpgradeType;

public class JohnDoe extends GameHandler {
	
	//Listas de control internas
	List<Integer> CCs;				//Lista para llevar el conteo de los CCs
	List<ArrayList<Unit>> VCEs;		//Lista para llevar el conteo de los VCEs de cada CC
	List<ArrayList<Integer>> trabajadoresMineral; //Lista para llevar el conteo de los VCEs que recolectan mineral de cada CC
	List<ArrayList<Integer>> trabajadoresVespeno; //Lista para llevar el conteo de los VCEs que recolectan vespeno de cada CC
	List<UnitType> unidadesPendientes; 	//Lista para llevar el conteo de las unidades entrenandose en este momento
	List<Unit> unidadesMilitares;		//Lista para llevar el conteo de todas las unidades militares entrenadas
	List<Unit> patrulla;				//Lista para saber las unidades que se han ido de fiesta
	List<Unit> tropaAsalto;				//Lista para saber cuales son los valientes que han ido a la guerra
	List<UnitType> edificiosPendientes;	//Lista para llevar el conteo de los edificios construyendose.
	List<Unit> edificiosConstruidos; 	//Lista para saber los edificios construidos actualmente
	
	int supplies, totalSupplies, barracones, refineria, fabricas, academia, arsenal;
	
	List<ChokePoint>[][] chokePoints;
	
	//Variables para tener controlado al trabajador seleccionado
	//el cc inicial y el cc del vce seleccionado.
	Unit worker, cc, cc_select;
	
	//Posici�n donde se va a construir el �ltimo edificio.
	Position posBuild;
	//Posici�n a la que mandar una patrulla.
	Position destination;
	Unit objetivo;
	
	Position tramo1;
	Position tramo2;
	
	int[][] mapa;
	
	HierarchicalMap hm;

	public JohnDoe(JNIBWAPI bwapi) {
		super(bwapi);
		
		worker = null;
		cc = null;
		cc_select = null;
		CCs 					= new ArrayList<Integer>();
		VCEs 					= new ArrayList<ArrayList<Unit>>();
		trabajadoresMineral 	= new ArrayList<ArrayList<Integer>>();
		trabajadoresVespeno 	= new ArrayList<ArrayList<Integer>>();
		unidadesPendientes 		= new ArrayList<UnitType>();
		unidadesMilitares		= new ArrayList<Unit>();
		patrulla				= new ArrayList<Unit>();
		tropaAsalto				= new ArrayList<Unit>();
		edificiosPendientes 	= new ArrayList<UnitType>();
		edificiosConstruidos 	= new ArrayList<Unit>();
		barracones = 0;
		refineria = 0;
		fabricas = 0;
		academia = 0;
		arsenal = 0;
		hm						= new HierarchicalMap((bwapi).getMap());
	}
	
	//A�aden las listas correspondientes al nuevo CC
	public void addCC(int cc_pos) {
		VCEs.add(new ArrayList<Unit>());
		trabajadoresMineral.add(new ArrayList<Integer>());
		trabajadoresVespeno.add(new ArrayList<Integer>());
	}
	
	//Obtiene un trabajador que se encuentra libre
	//Un trabajador est� libre cuando no recolecta mineral/vespeno o no hace nada con su vida
	public boolean getWorker() {
		for (ArrayList<Unit> vces_cc : VCEs) {
			for (Unit vce : vces_cc) {
				// Se comprueba si la unidades es de tipo VCE y no est� ocupada
				if ((!trabajadoresMineral.get(VCEs.indexOf(vces_cc)).contains(vce.getID()) &&
					 !trabajadoresVespeno.get(VCEs.indexOf(vces_cc)).contains(vce.getID())) &&
					 vce.isIdle() && vce.isCompleted()) {
					worker = vce;
					cc_select = this.connector.getUnit(CCs.get(VCEs.indexOf(vces_cc)));
					return true;
				}
			}
			
		}
		return false;
	}
	
	//Obtiene un trabajador para construir. Es diferente a obtener un trabajador, porque aqui cogemos un VCE
	//que est� recolectando minerales. Se construye siempre con un VCE del cc inicial.
	public boolean getMasterBuilder() {
		if (worker == null){
			//Se coge 1 VCE de la lista de VCEs del CC inicial (0)
			for (int vce : this.trabajadoresMineral.get(0)) {
				//Se pone como trabajador
				worker = this.connector.getUnit(vce);
				//Se elimina de la lista
				trabajadoresMineral.remove((Integer) vce);
				return true;
			}
			//No se ha podido seleccionar ninguno
			return false;
		}
		//ya hay uno cogido
		return true;
	}
	
	//Se manda a recolectar minerales al trabajador seleccionado, 
	// ya que antes de llamar a esta funci�n se llama a getWorker
	public boolean aCurrarMina(){
		//Se verifica que no se pase del n�mero de trabajadores y que el VCE est�
		//completado, ya que a veces se selecciona sin haber completado el entrenamiento.
		if ((trabajadoresMineral.get(CCs.indexOf(cc_select.getID())).size() < 15) && worker.isCompleted()){
			//Se buscan los minerales cercanos a la base.
			for (Unit recurso : this.connector.getNeutralUnits()) {
				if (recurso.getType().isMineralField()) {                                    
					double distance = cc_select.getDistance(recurso);                                    
					if (distance < 300) {
						//Se manda al VCE a recolectar
						this.connector.getUnit(worker.getID()).rightClick(recurso, false);
						trabajadoresMineral.get(CCs.indexOf(cc_select.getID())).add(worker.getID());
						worker = null;
						return true;
					}
				}
			}	
		}
		return false;
	}
	
	//Igual que los minerales
	public boolean aCurrarGas(){
		if (trabajadoresVespeno.get(CCs.indexOf(cc_select.getID())).size() < 2 && worker.isCompleted()) {
			for (Unit refineria : this.connector.getMyUnits()) {
				if (refineria.getType() == UnitTypes.Terran_Refinery && refineria.isCompleted()){
					this.connector.getUnit(worker.getID()).rightClick(refineria, false);
					trabajadoresVespeno.get(CCs.indexOf(cc_select.getID())).add(worker.getID());
					this.refineria = 1;
					worker = null;
					return true;	
				}
			}			
		}
		return false;
	}
	
	//Se comprueba si se posee m�s mineral y gas que el pasado por par�metro
	public boolean checkResources(int mineral, int gas){
		if (this.connector.getSelf().getMinerals() >= mineral &&
				this.connector.getSelf().getGas() >= gas){
			return true;
		}
		return false;
	}
	
	//Se comprueba si se puede construir una unidad
	public boolean canTrain(UnitType unidad) {
		if (this.connector.canMake(unidad)) {
			return true;
		}
		return false;

	}
	
	//Entrena una unidad 
	public boolean trainUnit(UnitType edificio, UnitType unidad){
		for (Unit u : edificiosConstruidos){
			if (u.getType() == edificio && (!unidadesPendientes.contains(unidad) && !u.isTraining())){
				u.train(unidad);
				unidadesPendientes.add(unidad);
			}
		}
		return false;
	}
	
	//Construye un edificio
	public boolean buildUnit(UnitType edificio) {
		if (edificiosPendientes.contains(edificio) || !worker.isExists()) {
			return false;
		}
		return worker.build(posBuild, edificio);
	}
	
	// Comprueba la posici�n de las unidades
	public boolean checkPositionUnits(){			
		for (Unit u : unidadesMilitares){
			if(u.isIdle() && u.isCompleted()){
				return true;
			}
		}
		return false;
	}
	
	// Selecciona las unidades militares que no est�n ocupadas para formar una patrulla
	public boolean chosseUnits(){
		if(patrulla.size()>2){ // La patrulla no necesita m�s de 3 unidades
			for (Unit u : patrulla){
				//System.out.println(u.isExists());
				if(u.isIdle()){
					return true;
				}
			}
			return false;
		}
		for (Unit u : unidadesMilitares){
			if(u.isIdle() && !patrulla.contains(u)){
				patrulla.add(u);
			}
		}
		if(!patrulla.isEmpty()){
			return true;
		}
		return false;
	}
	
	// Elige el destino de una patrulla
	public boolean chooseDestination(){
		Region p = this.connector.getMap().getRegion(cc.getPosition());
		Region objetivo = this.connector.getMap().getRegion((int)((Math.random()*(this.connector.getMap().getRegions().size()))));
		if(p.getAllConnectedRegions().contains(objetivo)){
			destination = objetivo.getCenter();
			return true;
		}
		return false;
	}
	
	// Mandar patrulla a la posicion destino
	public boolean sendUnits(){
		for(Unit soldadito : patrulla){
			if(soldadito.isIdle()){ // Solo mandamos a la unidad que este parada
				soldadito.move(destination, false);
			}
		}
		return true;
	}
	
	// Comprueba el estado de las unidades
	public boolean checkStateUnits(){
		for (Unit u : unidadesMilitares){
			if(u.isCompleted() && !patrulla.contains(u) && u.isIdle()){
				return true;
			}
		}
		return false;
	}
	
	// Selecciona las unidades militares para formar una tropa de asalto.
	public boolean chosseTropa(){
		for (Unit u : unidadesMilitares){
			if(u.isIdle() && !tropaAsalto.contains(u) && !patrulla.contains(u)){
				tropaAsalto.add(u);
			}
		}
		if(!tropaAsalto.isEmpty()){
			return true;
		}
		return false;
	}
	
	// Elige el el lugar o unidad a la que atacar
	public boolean chooseVictim(){
		if(tropaAsalto.size() > 10 && !this.connector.getEnemyUnits().isEmpty()){ // Atacamos base enemiga
			if(!this.connector.getEnemyUnits().isEmpty()){
				for(Unit victima : this.connector.getEnemyUnits()){
					if(victima.isExists()){
						objetivo = victima;
						return true;
					}
				}
			}
		}
		else{ // Defendemos nuestra base
			Region p = this.connector.getMap().getRegion(cc.getPosition());
			for(ChokePoint a : p.getChokePoints()){
				tramo1 = a.getFirstSide();
				tramo2 = a.getSecondSide();
				return true;
			}
		}
		return false;
	}
	
	// Mandar patrulla a la posicion destino
	public boolean sendAttack(){
		if(tropaAsalto.size() > 10 && !this.connector.getEnemyUnits().isEmpty()){ // Atacamos base enemiga
			for(Unit soldadito : tropaAsalto){
				soldadito.attack(objetivo, false);
			}
		}
		else{ // Defendemos nuestra base
			for(Unit soldadito : tropaAsalto){
				if(soldadito.isIdle()){
					soldadito.move(tramo1, false);
					soldadito.patrol(tramo2, true);
				}
			}
			return true;
				
		}
		
		return false;
	}
	
	
	/* Para construir vamos a coger como origen el CC. Y se realizar�n hasta 10 intentos
	 * para encontrar una posici�n v�lida. En cada intento se va cambiando de posici�n m�xima.
	 * (es decir, se va moviendo como un reloj para buscar la posici�n)
	 */
	public boolean findPosition(UnitType edificio) {
		//Caso especial de que sea una refiner�a
		if (edificio == UnitTypes.Terran_Refinery) {
			for (Unit vespeno : this.connector.getNeutralUnits()){
				if (vespeno.getType() == UnitTypes.Resource_Vespene_Geyser && vespeno.isVisible()){                                
						//Se obtiene la pos. del vespeno.
						posBuild = vespeno.getTopLeft();
						return true;
				}
			}
			//No se encuentra para una refiner�a, asique fuera
			return false;
		}
		//Caso especial de que sea una expansi�n
		if (edificio == UnitTypes.Terran_Command_Center) {
			for (BaseLocation pos : this.connector.getMap().getBaseLocations()) {
				if (this.connector.canBuildHere(pos.getPosition(), edificio, false)) {
					posBuild = pos.getPosition();
					return true;
				}
			}
			//No se encuentra para un CC, asique fuera
			return false;
		}
		//Edificios no especiales
		int [][] pruebas = {{1,1},{-1,-1},{-1,1},{1,-1}};
		for (int i=1; i<15; i++){
			for (int j=0; j<4; j++) {
				//Point origen, Point maximo, UnitType building
				Position pos = findPlace(new Point(cc.getTilePosition().getBX(), cc.getTilePosition().getBY()),
						new Point((cc.getTilePosition().getBX()+cc.getType().getTileWidth()+edificio.getTileWidth()*pruebas[j][0]*i),
								(cc.getTilePosition().getBY()+cc.getType().getTileHeight()+edificio.getTileHeight()*pruebas[j][1]*i)),
						edificio);
				//Si la posici�n es v�lida...
				if (pos.getBX() >= 0 && this.connector.canBuildHere(pos, edificio, false)){
					posBuild = pos;
					return true;
				}				
			}
		}
		//No se encuentra nada
		return false;
	}
	
	/**
     * M�todo que genera un mapa con los tama�os m�ximos
     * de edificios que se pueden construir desde una casilla
     * hacia abajo a la derecha.
     * 
     * V -> Vespeno
     * M -> Minerales
     * 0 -> No se puede construir
     * 
     * Se genera una posici�n inicial (0,0) y se va recorriendo
     * todo el mapa mirando si es construible/caminable una posici�n
     * 
     * Una vez se tiene generado el mapa se miran todos los recursos
     * y se sit�an en el mapa.
     */
    public void createMap() {
    	//La posici�n hay que desplazarla de 32 en 32 (tama�o de las casillas)
    	Position pos_aux = new Position(0,0, PosType.BUILD);
    	//Altura m�xima del mapa en pixeles
		int maxHeight = this.connector.getMap().getSize().getBY();
		//Anchura m�xima del mapa en pixeles
		int maxWidth = this.connector.getMap().getSize().getBX();
		//Altura de la casilla actual
		int altura = this.connector.getMap().getGroundHeight(pos_aux);
		//Mapa a devolver
		mapa = new int[maxHeight][maxWidth];
		//Tama�o m�ximo del edificio que se puede construir
		int dimension;
		//Variable que detiene la b�squeda si se encuentra un obst�culo
		boolean zonaLibre;
		//Indica si la siguiente posici�n es un cambio de altura
		
		// new Position(c,f, PosType.BUILD) 
		for(int f = 0; f < maxHeight; f++){
			for(int c = 0; c < maxWidth; c++){
				pos_aux = new Position(c, f, PosType.BUILD);
				dimension = 0;
				zonaLibre = true;
				
				//para cada posici�n se mira si se est� en los l�mites del mapa y
				//hay que verificar si la posici�n es construible
				if (f+1>=maxHeight || c+1>=maxWidth){
					zonaLibre = false;
				}
				if (!this.connector.getMap().isBuildable(pos_aux)){
					zonaLibre = false;
				}
				//Se obtiene la altura de la posici�n
				altura = this.connector.getMap().getGroundHeight(pos_aux);
				while(zonaLibre && dimension <= 4){
					dimension++;
					//Se verifica vertical, horizontal y diagonalmente si son v�lidas las posiciones.
					//Si alguna no lo es, se sale del while y se guarda el valor en el mapa
					for(int i = 0; i < dimension; i++){
						//matriz[i+f][c+dimension]	Comprueba columnas
						if (this.connector.isBuildable(new Position(c+dimension, f+i, PosType.BUILD), true)){ // �Es construible?
							if(this.connector.getMap().getGroundHeight(new Position(c+dimension, f+i, PosType.BUILD)) != altura){ // �Est�n a diferente altura?
								zonaLibre = false;
							}
						}
						else{
							zonaLibre = false;
						}
						
						//matriz[f+dimension][i+c]    Comprueba filas
						if (this.connector.isBuildable(new Position(c+i, f+dimension, PosType.BUILD), true)) {
							if(this.connector.getMap().getGroundHeight(new Position(c+i, f+dimension, PosType.BUILD)) != altura){
								zonaLibre = false;
							}
						}
						else{
							zonaLibre = false;
						}
					
						//matriz[f+dimension][c+dimension]   Comprueba en la diagonal (se podr�a cambiar cambiando la condicion del for a <=)
						if (this.connector.isBuildable(new Position(c+dimension, f+dimension, PosType.BUILD), true)) {
							if(this.connector.getMap().getGroundHeight(new Position(c+dimension, f+dimension, PosType.BUILD)) != altura){
								zonaLibre = false;
							}
						}
						else{
							zonaLibre = false;
						}
					}
					//si se est� en los l�mites del mapa en la pr�xima iteraci�n, se sale del bucle.  //creo que no hace falta porque isBuildable ya lo comprueba
					if (f+1+dimension>=maxHeight || c+1+dimension>=maxWidth){
						zonaLibre = false;
					}

				}
				// Se resta 1 porque hemos aumentado en 1 la dimensi�n suponiendo que la siguiente posici�n es v�lida
				if (dimension != 0)
					dimension--;
				// Se actualiza la posici�n
				mapa[f][c] = (dimension);	
			}
		}
		
		// Ahora se buscan los nodos de recursos y se les pone valores especiales:
		// -1 Para minerales
		// -2 Para vespeno.
		// getTilePosition devuelve la posici�n superior izquierda
		for (Unit u : this.connector.getNeutralUnits()){
			if (u.getType() == UnitTypes.Resource_Mineral_Field ||
					u.getType() == UnitTypes.Resource_Mineral_Field_Type_2 ||
					u.getType() == UnitTypes.Resource_Mineral_Field_Type_3) {
				//para recolectar minerales vale con que el vce vaya a cualquiera de sus casillas.
				mapa[u.getTilePosition().getBY()][u.getTilePosition().getBX()] = -1;
			}
			if (u.getType() == UnitTypes.Resource_Vespene_Geyser) {
				//Para construir la refiner�a nos vale la casilla arriba a la izquierda.
				mapa[u.getTilePosition().getBY()][u.getTilePosition().getBX()] = -2;
			}
		}
	}
    
    /**
     * Para poder crear la matriz deben ser en diagonal.
     * Para contemplar casos en el que origen y m�ximo sean en horizontal (mismo Y)
     * cuando ocurra eso, se toma Y como la Y del edificio. 
     * 
     * El m�todo devolver� un Position que indica la casilla superior izquierda v�lida donde construir el edificio.
     * Si se devuelve -1 en X no hay posici�n v�lida.
     */
    public Position findPlace(Point origen, Point maximo, UnitType building){
    	//Si no se pasan valores correctos, se devuelve posici�n inv�lida�
    	if (origen.x < 0 || origen.y < 0 ||
    			maximo.x < 0 || maximo.y < 0) {
    		return new Position(-2,0,PosType.BUILD);
    	}
    	
    	int xMaximo, xOrigen, yOrigen, yMaximo;
    	
    	//Limites de la submatriz X e Y
    	//Eje X
    	if (origen.x < maximo.x) {
    		//Origen est� antes que el maximo
    		xMaximo = (maximo.x > mapa[0].length ? mapa[0].length : maximo.x);
    		xOrigen = origen.x;
    	} else {
    		//Maximo est� antes que el origen
    		xMaximo = (origen.x > mapa[0].length ? mapa[0].length : origen.x);
    		xOrigen = maximo.x;
    	}
    	//Lo mismo con el eje Y
    	if (origen.y < maximo.y) {
    		yMaximo = (maximo.y > mapa.length ? mapa.length : maximo.y);
    		yOrigen = origen.y;
    	} else {
    		yMaximo = (origen.y > mapa.length ? mapa.length : origen.y);
    		yOrigen = maximo.y;
    	}
    	
    	//Valor a buscar de posiciones
    	int max = (building.getTileHeight() > building.getTileWidth()) ? building.getTileHeight() : building.getTileWidth();
    	//Variable de control para la b�squeda
    	boolean found = false;

    	//se recorre el mapa entre las posiciones dadas
    	for (; xOrigen < xMaximo && !found; xOrigen++){
    		for (; yOrigen < yMaximo && !found; yOrigen++){
    			//si encuentra una posici�n v�lida sale.
    			if (mapa[yOrigen][xOrigen] >= max) {
    				found = true;
    			}
    		}
    	}
    	
    	if (found) {
    		return new Position(xOrigen, yOrigen, PosType.BUILD);
    	} else {
    		return new Position(-1,0, PosType.BUILD);
    	}
    }
    
    /**
     * Se da por supuesto que las posiciones indicadas son posiciones correctas.
     * La posici�n origen ha sido obtenida mediante el m�todo findPlace y la posici�n
     * destino ha sido calculada con el tama�o del edificio + la posici�n origen
     */
    public void updateMap(Position origen, Position destino) {
    	//se recorre la matriz entre las posiciones dadas
    	for (int i = origen.getBY(); i < destino.getBY(); i++){
    		for(int j = origen.getBX(); j < destino.getBX(); j++){
    			//se ponen como ocupadas las casillas
    			mapa[i][j] = 0;
    		}
    	}
    	/*
    	 *  Para actualizar el resto de la matriz, tendremos que explorar las casillas superiores y por la izquierda.
    	 *  Dado que tambi�n hay que tener en cuenta las diagonales, se har� de tal forma que primero se actualicen
    	 *  todas las superiores incluidas las diagonales y despu�s las de la izquierda. 
    	 */
    	
    	// Esta variable se usar� para saber si hemos terminado la actualizaci�n
    	boolean parada = true;
    	// Esta variable servir� para desplazarnos verticalmente y tambien saber que dimensi�n maxima puede tener el edificio de esa casilla
    	int iv = 1;
    	// Esta variable servir� para desplazarnos horizontalmente
    	int ih = 0;
    	
    	// Bucle de actualizaci�n vertical
    	while (parada){
    		int extra = (destino.getBX()-origen.getBX() <= ih ? ih-(destino.getBX()-origen.getBX()) : 0);
    		//Si no nos salimos del mapa, el valor actual de la dimensi�n no es 4 (m�ximo)
    		if (((origen.getBY()-iv >= 0 && destino.getBX()-ih >= 0) && mapa[origen.getBY()-iv][destino.getBX()-ih] > iv) && (iv+extra < 4)){ // Si llegamos a 4 no es necesario seguir
    			mapa[origen.getBY()-iv][destino.getBX()-ih] = (iv == 1 ? iv+extra : iv);
    			iv++;
    		}
    		else{ // Hemos terminado con la columna, pasamos a la siguiente (hacia atr�s en el mapa)
    			if (iv == 1){
    				parada = false; // Si en la primera casilla no hay que actualizar, significa que hemos terminado.
    			}
    			else{
    				ih++;
        			iv = 1;
    			}
    		}
    	}
    	
    	ih = 1;
    	iv = 0;
  
    	parada = true;
    	// Bucle horizontal
    	while (parada){
    		if (((origen.getBY()+iv >= 0 && origen.getBX()-ih >= 0) && mapa[origen.getBY()+iv][origen.getBX()-ih] > ih) && (ih < 4)){ // Si llegamos a 4 no es necesario seguir
    			mapa[origen.getBY()+iv][origen.getBX()-ih] = ih;
    			ih++;
    		}
    		else{ // Hemos terminado con la fila, pasamos a la siguiente (hacia abajo en el mapa)
    			if (ih == 1 || origen.getBY()+iv == destino.getBY()){
    				parada = false; // Si en la primera casilla no hay que actualizar, significa que hemos terminado.
    			}
    			else{
    				iv++;
        			ih = 1;
    			}
    		}
    	}	
    }

}
