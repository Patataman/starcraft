package org.iaie.practica2;

import java.awt.Point;
import java.util.List;

public class IngluenceMap {

	double mapa[][];
	final int umbral = 3;
	final int radio = 2;
	
	public IngluenceMap(int alto, int ancho) {
		mapa = new double[alto][ancho]; 
	}
	
	
	/*
	 * Este m�etodo modificar�a el valor de la
	 * influencia de una casilla. Tiene dos par�ametros de entrada que se corresponden con la celda sobre la que se
	 * va a realizar la actualizaci�on mediante un objeto de tipo Point y el valor de influencia de la casilla que ser�a
	 * de tipo int. La modificaci�on de la influencia de una casilla se realizar�a mediante una operaci�on de adici�on.
	 * Hay que tener en cuenta que la modificaci�on de la influencia de una casilla supone tambi�en la propagaci�on
	 * de la influencia a las casillas que la rodean
	 */
	public void updateCellInfluence(Point punto, int influencia){
		// Optenemos el area de efecto limitada a la dimensi�n del mapa en esa posici�n.
		int n = ((int)punto.getY()-umbral>0)?(int)punto.getY()-umbral:0;
		int s = ((int)punto.getY()+umbral<mapa.length)?(int)punto.getY()+umbral:mapa.length;
		int w = ((int)punto.getX()-umbral>0)?(int)punto.getX()-umbral:0;
		int e = ((int)punto.getX()+umbral<mapa.length)?(int)punto.getX()+umbral:mapa[0].length;
		
		for(int i = n; i<s; i++){
			for(int j = w; j<e; j++){
				mapa[i][j] += influencia/Math.pow((1+distanciaEuclidea(punto, i, j)),2);
			}
		}
	}
	
	/*
	 * Este m�etodo modificar�a el valor de 
	 * influencia de un conjunto de casillas. Este m�etodo tiene dos par�ametros de entrada que se corresponden con
	 * un conjunto de celdas sobre las que se va a realizar la actualizaci�on mediante una lista de objetos de tipo
	 * Point y el valor de influencia para cada una de las casillas que ser�a un valor de tipo int. La modificaci�on de
	 * la influencia de cada una de las casillas se realizar�a mediante una llamada al m�etodo updateCellInfluence
	 */
	public void updateCellsInfluence(List<Point> puntos, int influencia){
		for(Point p : puntos){
			updateCellInfluence(p, influencia);
		}
	}
	
	/*
	 * Este m�etodo devolver�a el valor de influencia
	 * de una casilla. Este m�etodo tiene un par�ametro de entrada de tipo Point que se corresponde con la posici�on
	 * de la casilla de la que se quiere obtener el valor de influencia
	 */
	public double getInfluence(Point punto){	
		return mapa[(int)punto.getY()][(int)punto.getX()];
	}
	
	public int distanciaEuclidea(Point inicio, int i, int j){
		int h = Math.abs((int)inicio.getX() - j);
		int v = Math.abs((int)inicio.getY() - i);
		return (h>v)?h:v;
	}
	
	
	/*
	 * Este m�etodo deber�a calcular el valor
	 * de influencia del jugador sobre el mapa. El valor de influencia del jugador ser�a la suma de los valores de
	 * influencia de las casillas pertenecientes al jugador.
	 */
	public double getMyInfluenceLevel(){
		double InfluenceLevel = 0.0;
		for(int i = 0; i<mapa.length; i++){
			for(int j = 0; j<mapa[0].length; j++){
				if(mapa[i][j]>0){
					InfluenceLevel += mapa[i][j]; 
				}
			}
		}	
		return InfluenceLevel;		
	}


	/*
	 * Este m�etodo deber�a obtener el
	 * valor de influencia del jugador o jugadores enemigos sobre el mapa. El valor de influencia del enemigo ser�a la
	 * suma de los valores de influencia de las casillas pertenecientes al enemigo.
	 */
	public double getEnemyInfluenceLevel(){
		double InfluenceLevel = 0.0;
		for(int i = 0; i<mapa.length; i++){
			for(int j = 0; j<mapa[0].length; j++){
				if(mapa[i][j]<0){
					InfluenceLevel += mapa[i][j]; 
				}
			}
		}	
		return InfluenceLevel;
	}


	/*
	 *  Este m�etodo deber�a calcular el n�umero
	 *  e casillas sobre las cuales el jugador tiene el control. Se sumar�a 1 unidad por cada casilla perteneciente al
	 *  jugador.
	 */
	public int getMyInfluenceArea(){
		int count = 0;
		for(int i = 0; i<mapa.length; i++){
			for(int j = 0; j<mapa[0].length; j++){
				if(mapa[i][j]>0){
					count += 1; 
				}
			}
		}	
		return count;		
	}

	
	/*
	 * Este m�etodo deber�a calcular el
	 * n�umero de casillas sobre las cuales el jugador o jugadores enemigos tienen el control. Se sumar�a 1 unidad por
	 * cada casilla perteneciente al enemigo
	 */
	public int getEnemyInfluenceArea(){
		int count = 0;
		for(int i = 0; i<mapa.length; i++){
			for(int j = 0; j<mapa[0].length; j++){
				if(mapa[i][j]<0){
					count += 1; 
				}
			}
		}	
		return count;
		
	}
	
	/*
	 * PENDIENTE DE APROBACI�N Para calcular la influencia se llama a este m�todo con el type de la unidad.
	 */
}
