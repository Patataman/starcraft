package org.iaie.practica2;

import org.iaie.btree.state.State;
import org.iaie.btree.task.leaf.Action;
import org.iaie.btree.util.GameHandler;

public class ChosseTropa extends Action {

	public ChosseTropa(String name, GameHandler gh) {
		super(name, gh);
		// TODO Apéndice de constructor generado automáticamente
	}

	@Override
	public State execute() {
		try{
			if (((JohnDoe)this.handler).chosseTropa()) {
				return State.SUCCESS;
			} else {
				return State.FAILURE;
			}
		} catch (Exception e) {
			e.printStackTrace();
			return State.ERROR;
		}
	}

}
