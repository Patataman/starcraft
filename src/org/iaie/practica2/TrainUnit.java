package org.iaie.practica2;

import org.iaie.btree.state.State;
import org.iaie.btree.task.leaf.Action;
import org.iaie.btree.util.GameHandler;

import jnibwapi.Unit;
import jnibwapi.types.UnitType;
import jnibwapi.types.UnitType.UnitTypes;

public class TrainUnit extends Action {
	
	UnitType unit, building;
	
	public TrainUnit(String name, GameHandler gh, UnitType unit, UnitType building) {
		super(name, gh);
		this.unit = unit;
		this.building = building;
	}

	@Override
	public State execute() {
		try{
			int soldados=0, murcielagos=0, cont=0;
			for (Unit u : ((JohnDoe)this.handler).unidadesMilitares){
				if (u.getType() == UnitTypes.Terran_Marine) soldados++;
				if (u.getType() == UnitTypes.Terran_Firebat) murcielagos++;
			}
			if (unit == UnitTypes.Terran_SCV){
				//Se mira a ver si es posible entrenar alg�n VCE
				for (Integer cc : ((JohnDoe)this.handler).CCs) {
					if (((JohnDoe)this.handler).VCEs.get(((JohnDoe)this.handler).CCs.indexOf(cc)).size() > 18) {
						cont++;
					}
				}
				if (cont == ((JohnDoe)this.handler).VCEs.size()) {
					return State.FAILURE; 					
				}
			}
			//Por cada 5 soldados+murcielagos debe entrenarse un m�dico
			if (unit == UnitTypes.Terran_Medic && (soldados+murcielagos)%5 != 0) {
				return State.FAILURE;
			}
			//Por cada 3 soldados 1 murcielago debe entrenarse
			if (unit == UnitTypes.Terran_Firebat && (soldados+murcielagos)%3 != 0) {
				return State.FAILURE;
			}
			//Los goliats, mientras mas mejor
			if (((JohnDoe)this.handler).trainUnit(building, unit)) {
				return State.SUCCESS;
			} else {
				return State.FAILURE;
			}
		} catch (Exception e) {
			e.printStackTrace();
			return State.ERROR;
		}
	}

}
