package org.iaie.practica2;

import java.util.ArrayList;
import java.util.function.Predicate;

import org.iaie.Agent;
import org.iaie.btree.BehavioralTree;
import org.iaie.btree.task.composite.Selector;
import org.iaie.btree.task.composite.Sequence;
import org.iaie.btree.util.GameHandler;
import org.iaie.tools.Options;

import jnibwapi.BWAPIEventListener;
import jnibwapi.JNIBWAPI;
import jnibwapi.Position;
import jnibwapi.Unit;
import jnibwapi.Position.PosType;
import jnibwapi.types.UnitType.UnitTypes;
import jnibwapi.types.UpgradeType.UpgradeTypes;
import jnibwapi.types.UpgradeType;

public class PlayerPractica203168900315536 extends Agent implements BWAPIEventListener {

	BehavioralTree CollectTree;
	Unit buildingTree;
	JohnDoe gh;
	
	public PlayerPractica203168900315536() {            

        // Generaci�n del objeto de tipo agente

        // Creaci�n de la superclase Agent de la que extiende el agente, en este m�todo se cargan            
        // ciertas variables de de control referentes a los par�metros que han sido introducidos 
        // por teclado. 
        super();
        // Creaci�n de una instancia del connector JNIBWAPI. Esta instancia s�lo puede ser creada
        // una vez ya que ha sido desarrollada mediante la utilizaci�n del patr�n de dise�o singlenton.
        this.bwapi = new JNIBWAPI(this, true);
        // Inicia la conexi�n en modo cliente con el servidor BWAPI que est� conectado directamente al videojuego.
        // Este proceso crea una conexi�n mediante el uso de socket TCP con el servidor. 
        this.bwapi.start();
    }
	
	@Override
	public void connected() {
	}

	@Override
	public void matchStart() {
		 
        // Mediante est� metodo se puede obtener informaci�n del usuario. 
        if (Options.getInstance().getUserInput()) this.bwapi.enableUserInput();

        if (Options.getInstance().getInformation()) this.bwapi.enablePerfectInformation();
        //AL QUITARLE INFORMACI�N COMPLETA CONSTRUYE BIEN LA REFINERIA
        // Mediante este m�todo se define la velocidad de ejecuci�n del videojuego. 
        // Los valores posibles van desde 0 (velocidad est�ndar) a 10 (velocidad m�xima).
        this.bwapi.setGameSpeed(Options.getInstance().getSpeed());
		
		gh = new JohnDoe(bwapi);
		
		//Se establece la variable del centro de mando,
		//ya que se va a usar bastante y as� evitamos recorrer
		//la lista de myUnits
		for (Unit cc : bwapi.getMyUnits()){
			if (cc.getType() == UnitTypes.Terran_Command_Center){
				gh.cc = cc;
				gh.cc_select = cc;
				gh.CCs.add(cc.getID());
				gh.addCC(0);
				gh.edificiosConstruidos.add(cc);
			}
		}

		gh.supplies = bwapi.getSelf().getSupplyUsed();
		gh.totalSupplies = bwapi.getSelf().getSupplyTotal();
		//gh.addCC(gh.CCs.indexOf(gh.cc_select.getID()));
		
		gh.createMap();
		
		Selector<GameHandler> CollectResources = new Selector<>("Minerales o Vespeno");
		CollectResources.addChild(new CollectMineral("Minerales", gh));
		CollectResources.addChild(new CollectGas("Vespeno", gh));
		
		Sequence Collect = new Sequence("Recolectar");
		Collect.addChild(new FreeWorker("Trabajador libre", gh));
		Collect.addChild(CollectResources);
		
		// -------- Secuencias de entrenamiento ---------
		
		//Entrenar VCEs
		Sequence TrainVCE = new Sequence("Entrenar VCE");
		TrainVCE.addChild(new CheckResources("Comprobar recursos vce", gh, UnitTypes.Terran_SCV));
		TrainVCE.addChild(new ChooseBuilding("Comprobar crear VCE", gh, UnitTypes.Terran_SCV));
		TrainVCE.addChild(new TrainUnit("Entrenar VCE", gh, UnitTypes.Terran_SCV, UnitTypes.Terran_Command_Center));
		//Entrenar soldados
		Sequence TrainMarine = new Sequence("Entrenar Soldado");
		TrainMarine.addChild(new CheckResources("Comprobar recursos soldado", gh, UnitTypes.Terran_Marine));
		TrainMarine.addChild(new ChooseBuilding("Comprobar entrenamiento soldado", gh, UnitTypes.Terran_Marine));
		TrainMarine.addChild(new TrainUnit("Entrenar soldado", gh, UnitTypes.Terran_Marine, UnitTypes.Terran_Barracks));
		//Entrenar medicos
		Sequence TrainMedic = new Sequence("Entrenar Medico");
		TrainMedic.addChild(new CheckResources("Comprobar recursos medico", gh, UnitTypes.Terran_Medic));
		TrainMedic.addChild(new ChooseBuilding("Comprobar entrenamiento medico", gh, UnitTypes.Terran_Medic));
		TrainMedic.addChild(new TrainUnit("Entrenar medico", gh, UnitTypes.Terran_Medic, UnitTypes.Terran_Barracks));
		//Entrenar murcielagos de fuego
		Sequence TrainFirebat = new Sequence("Entrenar Firebat");
		TrainFirebat.addChild(new CheckResources("Comprobar recursos murcielago", gh, UnitTypes.Terran_Firebat));
		TrainFirebat.addChild(new ChooseBuilding("Comprobar entrenamiento murcielago", gh, UnitTypes.Terran_Firebat));
		TrainFirebat.addChild(new TrainUnit("Entrenar murcielago", gh, UnitTypes.Terran_Firebat, UnitTypes.Terran_Barracks));
		//Entrenar goliats
		Sequence TrainGoliat = new Sequence("Entrenar Goliat");
		TrainGoliat.addChild(new CheckResources("Comprobar recursos goliat", gh, UnitTypes.Terran_Goliath));
		TrainGoliat.addChild(new ChooseBuilding("Comprobar entrenamiento goliat", gh, UnitTypes.Terran_Goliath));
		TrainGoliat.addChild(new TrainUnit("Entrenar goliat", gh, UnitTypes.Terran_Goliath, UnitTypes.Terran_Factory));
		//Selector con todos los posibles entrenamientos
		Selector selectorTrain = new Selector<>("Selector train", TrainGoliat, TrainMedic, TrainFirebat, TrainMarine, TrainVCE);
		
		// ----------- FIN TRAIN ---------

		
		// -------- Secuencias de construcci�n ---------
		//Construir dep�sito de suministros
		Sequence buildSupply = new Sequence("Construir suministros");
		buildSupply.addChild(new CheckResources("Comprobar recursos suministros", gh, UnitTypes.Terran_Supply_Depot));
		buildSupply.addChild(new FindPosition("Encontrar posicion", gh, UnitTypes.Terran_Supply_Depot));
		buildSupply.addChild(new FreeBuilder("Encontrar un constructor", gh));
		buildSupply.addChild(new Build("Construir suministros", gh, UnitTypes.Terran_Supply_Depot));
		//Construir barracones
		Sequence buildBarracks = new Sequence("Construir barracones");
		buildBarracks.addChild(new CheckResources("Comprobar recursos barracones", gh, UnitTypes.Terran_Barracks));
		buildBarracks.addChild(new FindPosition("Encontrar posicion", gh, UnitTypes.Terran_Barracks));
		buildBarracks.addChild(new FreeBuilder("Encontrar un constructor", gh));
		buildBarracks.addChild(new Build("Construir barracones", gh, UnitTypes.Terran_Barracks));
		//Construir refineria
		Sequence buildRefinery = new Sequence("Construir refineria");
		buildRefinery.addChild(new CheckResources("Comprobar recursos refineria", gh, UnitTypes.Terran_Refinery));
		buildRefinery.addChild(new FindPosition("Encontrar posicion", gh, UnitTypes.Terran_Refinery));
		buildRefinery.addChild(new FreeBuilder("Encontrar un constructor", gh));
		buildRefinery.addChild(new Build("Construir refineria", gh, UnitTypes.Terran_Refinery));
		//Construir bah�a de ingenieria
		Sequence buildBay = new Sequence("Construir bahia");
		buildBay.addChild(new CheckResources("Comprobar recursos bahia", gh, UnitTypes.Terran_Engineering_Bay));
		buildBay.addChild(new FindPosition("Encontrar posicion", gh, UnitTypes.Terran_Engineering_Bay));
		buildBay.addChild(new FreeBuilder("Encontrar un constructor", gh));
		buildBay.addChild(new Build("Construir bahia", gh, UnitTypes.Terran_Engineering_Bay));
		//Construir academia
		Sequence buildAcademy = new Sequence("Construir academia");
		buildAcademy.addChild(new CheckResources("Comprobar recursos academia", gh, UnitTypes.Terran_Academy));
		buildAcademy.addChild(new FindPosition("Encontrar posicion", gh, UnitTypes.Terran_Academy));
		buildAcademy.addChild(new FreeBuilder("Encontrar un constructor", gh));
		buildAcademy.addChild(new Build("Construir academia", gh, UnitTypes.Terran_Academy));
		buildRefinery.addChild(new Build("Construir refineria", gh, UnitTypes.Terran_Refinery));
		//Construir fabrica
		Sequence buildFactory = new Sequence("Construir fabrica");
		buildFactory.addChild(new CheckResources("Comprobar recursos fabrica", gh, UnitTypes.Terran_Factory));
		buildFactory.addChild(new FindPosition("Encontrar posicion", gh, UnitTypes.Terran_Factory));
		buildFactory.addChild(new FreeBuilder("Encontrar un constructor", gh));
		buildFactory.addChild(new Build("Construir fabrica", gh, UnitTypes.Terran_Factory));
		//Construir fabrica
		Sequence buildArmory = new Sequence("Construir arsenal");
		buildArmory.addChild(new CheckResources("Comprobar recursos arsenal", gh, UnitTypes.Terran_Armory));
		buildArmory.addChild(new FindPosition("Encontrar posicion", gh, UnitTypes.Terran_Armory));
		buildArmory.addChild(new FreeBuilder("Encontrar un constructor", gh));
		buildArmory.addChild(new Build("Construir arsenal", gh, UnitTypes.Terran_Armory));
		//Construir CC
		Sequence buildCC = new Sequence("Construir centro de mando");
		buildCC.addChild(new CheckResources("Comprobar recursos CC", gh, UnitTypes.Terran_Command_Center));
		buildCC.addChild(new FindPosition("Encontrar posicion", gh, UnitTypes.Terran_Command_Center));
		buildCC.addChild(new FreeBuilder("Encontrar un constructor", gh));
		buildCC.addChild(new Build("Construir CC", gh, UnitTypes.Terran_Command_Center));
		
		Selector selectorBuild = new Selector<>("Selector build", buildSupply, buildBarracks, 
												buildRefinery, buildBay, buildAcademy, buildCC, buildFactory, buildArmory);
		// ---------- FIN BUILD -----------
		
		// -------- Secuencias de movimiento ---------
		Sequence adventure = new Sequence("Mover unidades");
		adventure.addChild(new CheckPositionUnits("Comprobar posici�n de las unidades", gh));
		adventure.addChild(new ChosseUnits("Formar patrulla", gh));
		adventure.addChild(new ChooseDestination("Escoger destino", gh));
		adventure.addChild(new SendUnits("Mandar patrulla", gh));
		// ---------- FIN MOVE -----------
		
		// -------- Secuencias de ataque ---------
		Sequence attack = new Sequence("Atacar");
		attack.addChild(new CheckStateUnits("Comprobar estado de las unidades", gh));
		attack.addChild(new ChosseTropa("Formar tropa", gh));
		attack.addChild(new ChooseVictim("Escoger v�ctima", gh));
		attack.addChild(new SendAttack("Mandar ataque", gh));
		// ---------- FIN ATTACK -----------
		
		CollectTree = new BehavioralTree("Primer arbol");
		CollectTree.addChild(new Selector<>("MAIN SELECTOR", Collect, selectorBuild, selectorTrain, adventure, attack));
		
		
	}

	@Override
	public void matchFrame() {
		CollectTree.run();
	}

	@Override
	public void matchEnd(boolean winner) {
	}

	@Override
	public void keyPressed(int keyCode) {
	}

	@Override
	public void sendText(String text) {
	}

	@Override
	public void receiveText(String text) {
	}

	@Override
	public void playerLeft(int playerID) {
	}

	@Override
	public void nukeDetect(Position p) {
	}

	@Override
	public void nukeDetect() {
	}

	@Override
	public void unitDiscover(int unitID) {
	}

	@Override
	public void unitEvade(int unitID) {
	}

	@Override
	public void unitShow(int unitID) {
	}

	@Override
	public void unitHide(int unitID) {
	}

	@Override
	public void unitCreate(int unitID) {
		//Cuando se comienza a construir un edificio se pone como pendiente.
		if (bwapi.getUnit(unitID).getPlayer().getID() == bwapi.getSelf().getID()) {
			if (bwapi.getUnit(unitID).getType().isBuilding()){
				gh.edificiosPendientes.add(bwapi.getUnit(unitID).getType());
			}
		}
	}
	

	@Override
	public void unitDestroy(int unitID) {
		Predicate<Unit> predicado = new Predicate<Unit>() {
			public boolean test(Unit u) {
				return u.getID() == unitID;
				
			}
		};
		int control = 0;
		if (control == 0) {
			//Casting a array de unidades (?)
			for(Object u : gh.edificiosConstruidos.stream().filter(predicado).toArray()) {
				//No es necesario comprobar el ID ya que la sublista que se recorre es la que cumple lo del ID
				//Aunque s�lo deber�a haber 1 elemento
				gh.edificiosConstruidos.remove(u);
				if (((Unit) u).getType() == UnitTypes.Terran_Academy) gh.academia--;
				if (((Unit) u).getType() == UnitTypes.Terran_Barracks) gh.barracones--;
				if (((Unit) u).getType() == UnitTypes.Terran_Factory) gh.fabricas--;
				if (((Unit) u).getType() == UnitTypes.Terran_Armory) gh.arsenal--;
				if (((Unit) u).getType() == UnitTypes.Terran_Refinery) gh.refineria--;
				if (((Unit) u).getType() == UnitTypes.Terran_Supply_Depot) {
					gh.totalSupplies -= UnitTypes.Terran_Supply_Depot.getSupplyProvided();
				}
				if (((Unit) u).getType() == UnitTypes.Terran_Command_Center) {
					gh.CCs.remove((Integer) unitID);
				}
				control++;
			}
		}
		if (control == 0) {
			for(Object u : gh.unidadesMilitares.stream().filter(predicado).toArray()) {
				gh.unidadesMilitares.remove(u);
				control++;
			}
			for(Object u : gh.patrulla.stream().filter(predicado).toArray()) {
				gh.patrulla.remove(u);
				control++;
			}
			for(Object u : gh.tropaAsalto.stream().filter(predicado).toArray()) {
				gh.tropaAsalto.remove(u);
				control++;				
			}
		}
		if (control == 0) {
			for(ArrayList<Unit> vces_cc : gh.VCEs){
				for(Object u : vces_cc.stream().filter(predicado).toArray()) {
					gh.VCEs.get(gh.VCEs.indexOf(vces_cc)).remove((Unit) u);									
					control++;
				}
			}
		}

		if (control == 0) {
			for(ArrayList<Integer> minerales : gh.trabajadoresMineral) {
				if (minerales.contains((Integer) unitID)) {
					gh.trabajadoresMineral.get(gh.trabajadoresMineral.indexOf(minerales)).remove((Integer) unitID);
					control++;
				}
			}
		}
		if (control == 0) {
			for(ArrayList<Integer> vespeno : gh.trabajadoresVespeno) {
				if (vespeno.contains((Integer) unitID)) {
					gh.trabajadoresVespeno.get(gh.trabajadoresVespeno.indexOf(vespeno)).remove((Integer) unitID);
				}
			}
		}
	}

	@Override
	public void unitMorph(int unitID) {
	}

	@Override
	public void unitRenegade(int unitID) {
	}

	@Override
	public void saveGame(String gameName) {
	}

	@Override
	public void unitComplete(int unitID) {
		if (bwapi.getUnit(unitID).getPlayer().getID() == bwapi.getSelf().getID()) {
			//Cuando se finaliza la unidad correspondiente, se agrega a su lista.
			if (bwapi.getUnit(unitID).getType() == UnitTypes.Terran_SCV){
				gh.VCEs.get(gh.CCs.indexOf(gh.cc_select.getID())).add(bwapi.getUnit(unitID));
			}
			//Cuando se cree una unidad de las pendientes, se elimina de la lista.
			if (gh.unidadesPendientes.contains(bwapi.getUnit(unitID).getType())){
				gh.unidadesPendientes.remove(bwapi.getUnit(unitID).getType());
				gh.supplies += bwapi.getUnit(unitID).getType().getSupplyRequired();
				//Los terran s�lo poseen 1 unidad no militar, los VCEs.
				if (bwapi.getUnit(unitID).getType() != UnitTypes.Terran_SCV) {
					gh.unidadesMilitares.add(bwapi.getUnit(unitID));
				}
			}
			//Cuando se cree un edificio pendiente, se elimina de la lista y se pone como construido
			if (gh.edificiosPendientes.contains(bwapi.getUnit(unitID).getType())) {
				gh.edificiosPendientes.remove(bwapi.getUnit(unitID).getType());
				gh.edificiosConstruidos.add(bwapi.getUnit(unitID));
				if (bwapi.getUnit(unitID).getType() == UnitTypes.Terran_Supply_Depot)
					gh.totalSupplies += UnitTypes.Terran_Supply_Depot.getSupplyProvided();
				if (bwapi.getUnit(unitID).getType() == UnitTypes.Terran_Barracks) gh.barracones++;
				if (bwapi.getUnit(unitID).getType() == UnitTypes.Terran_Refinery) gh.refineria++;
				if (bwapi.getUnit(unitID).getType() == UnitTypes.Terran_Academy) gh.academia++;
				if (bwapi.getUnit(unitID).getType() == UnitTypes.Terran_Factory) gh.fabricas++;
				if (bwapi.getUnit(unitID).getType() == UnitTypes.Terran_Armory) gh.arsenal++;
				if (bwapi.getUnit(unitID).getType() == UnitTypes.Terran_Command_Center) {
					if (gh.CCs.indexOf((Integer) unitID) == -1){
						gh.CCs.add(unitID);
						gh.addCC(gh.CCs.indexOf(unitID));
					}
				}
				
				//Se actualiza el mapa.
				gh.updateMap(bwapi.getUnit(unitID).getTopLeft(),
						new Position(bwapi.getUnit(unitID).getTopLeft().getBX()+bwapi.getUnit(unitID).getType().getTileWidth(),
									bwapi.getUnit(unitID).getTopLeft().getBY()+bwapi.getUnit(unitID).getType().getTileHeight(),
									PosType.BUILD));
			}
		}
	}

	@Override
	public void playerDropped(int playerID) {
		// TODO Auto-generated method stub

	}

}
