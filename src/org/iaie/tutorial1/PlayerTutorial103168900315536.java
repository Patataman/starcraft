package org.iaie.tutorial1;

import jnibwapi.BWAPIEventListener;
import jnibwapi.JNIBWAPI;
import jnibwapi.Position;
import jnibwapi.Unit;
import jnibwapi.types.TechType;
import jnibwapi.types.TechType.TechTypes;
import jnibwapi.types.UnitType;
import jnibwapi.types.UnitType.UnitTypes;
import jnibwapi.types.UpgradeType;
import jnibwapi.types.UpgradeType.UpgradeTypes;
import org.iaie.Agent;
import org.iaie.tools.Options;


public class PlayerTutorial103168900315536 extends Agent implements BWAPIEventListener {

	/** Esta variable se usa para almacenar aquellos depositos de minerales 
     *  que han sido seleccionados para ser explotados por las unidades 
     *  recolectoras. */
    //private final HashSet<Unit> claimedMinerals = new HashSet<>();
	//SE IGNORA PORQUE YA SE CONTROLAN LOS TRABAJADORES EN LOS MINERALES AL CREARLOS

    /** Esta variable se utiliza la generación de VCEs */
    private boolean scvReady;

    /** Esta variable se utiliza como contenedor temporal para selecionar
     *  un vce que va a ponerse a trabajar.*/
    private Unit scvWorking;
    private Unit commandCenter;

    /** Esta variable se utiliza para comprobar cuando debe se debe construir
     * un nuevo depósito de suministros.*/
    private int supplyCap;
    
    private byte VCEs;
   
    private byte barracones;

    public PlayerTutorial103168900315536() {            

        // Generación del objeto de tipo agente

        // Creación de la superclase Agent de la que extiende el agente, en este método se cargan            
        // ciertas variables de de control referentes a los parámetros que han sido introducidos 
        // por teclado. 
        super();
        // Creación de una instancia del connector JNIBWAPI. Esta instancia sólo puede ser creada
        // una vez ya que ha sido desarrollada mediante la utilización del patrón de diseño singlenton.
        this.bwapi = new JNIBWAPI(this, true);
        // Inicia la conexión en modo cliente con el servidor BWAPI que está conectado directamente al videojuego.
        // Este proceso crea una conexión mediante el uso de socket TCP con el servidor. 
        this.bwapi.start();
    }

    /**
     * Este evento se ejecuta una vez que la conexión con BWAPI se ha estabilidad. 
     */
    @Override
    public void connected() {
        System.out.println("IAIE: Conectando con BWAPI");
    }

    /**
     * Este evento se ejecuta al inicio del juego una única vez, en el se definen ciertas propiedades
     * que han sido leidas como parámetros de entrada.
     * Velocidad del juego (Game Speed): Determina la velocidad a la que se ejecuta el videojuego. Cuando el juego 
     * se ejecuta a máxima velocidad algunas eventos pueden ser detectados posteriormente a su ejecución real. Esto
     * es debido a los retrasos en las comunicacion y el retardo producido por la tiempo de ejecución del agente. En 
     * caso de no introducir ningun valor el jugador 
     * Información perfecta (Perfect información): Determina si el agente puede recibir información completa del 
     * juego. Se consedira como información perfecta cuando un jugador tiene acceso a toda la información del entorno, 
     * es decir no le afecta la niebla de guerra.
     * Entrada de usuarios (UserInput)
     */
    @Override
    public void matchStart() {

        System.out.println("IAIE: Iniciando juego");

        // Revisar. 
        // Mediante esté metodo se puede obtener información del usuario. 
        if (Options.getInstance().getUserInput()) this.bwapi.enableUserInput();
        // Mediante este método se activa la recepción completa de información.
        if (Options.getInstance().getInformation()) this.bwapi.enablePerfectInformation();
        // Mediante este método se define la velocidad de ejecución del videojuego. 
        // Los valores posibles van desde 0 (velocidad estándar) a 10 (velocidad máxima).
        this.bwapi.setGameSpeed(Options.getInstance().getSpeed());

        // Iniciamos las variables de control
        // Se establece el contador de objetos a cero y se eliminan todas las
        // referencias previas a los objetos anteriormente añadidos.
        //claimedMinerals.clear();
        // Se establecen los valores por defecto de las variables de control.
        scvReady = false;
        scvWorking = null;
        supplyCap = 0;
        VCEs = 4;
        barracones = 0;
        
    	for (Unit cc : this.bwapi.getMyUnits()){
			if (cc.getType() == UnitTypes.Terran_Command_Center) {
				commandCenter = cc;
			}
			
		}
    }

    /**
     * Evento Maestro
     */
    @Override
    public void matchFrame() {

        String msg = "=";
        // Mediante este bucle se comprueba si el jugador está investigando
        // algún tipo de tecnología y lo muestra por pantalla
        for (TechType t : TechTypes.getAllTechTypes()) {
            if (this.bwapi.getSelf().isResearching(t)) {
                msg += "Investigando " + t.getName() + "=";
            }
            // Exclude tech that is given at the start of the game
            UnitType whatResearches = t.getWhatResearches();
            if (whatResearches == UnitTypes.None) {
                continue;
            }
            if (this.bwapi.getSelf().isResearched(t)) {
                msg += "Investigado " + t.getName() + "=";
            }
        }

        // Mediante este bucle se comprueba si se está realizando una actualización
        // sobre algún tipo de unidad. 
        for (UpgradeType t : UpgradeTypes.getAllUpgradeTypes()) {
            if (this.bwapi.getSelf().isUpgrading(t)) {
                msg += "Actualizando " + t.getName() + "=";
            }
            if (this.bwapi.getSelf().getUpgradeLevel(t) > 0) {
                int level = this.bwapi.getSelf().getUpgradeLevel(t);
                msg += "Actualizado " + t.getName() + " a nivel " + level + "=";
            }
        }
        
        //Bucle para averiguar cuantos barracones se tienen.
        //Algo guarro, pero es lo que hay.
        barracones = 0;
        for (Unit building : this.bwapi.getMyUnits()){
        	if (building.getType() == UnitTypes.Terran_Barracks){
        		barracones += 1;
        	}
        }

        this.bwapi.drawText(new Position(0, 20), msg, true);

        // Mediante este método se 
        this.bwapi.getMap().drawTerrainData(bwapi);

        if (scvWorking != null && (scvWorking.isIdle() || scvWorking.isGatheringMinerals()
        							|| scvWorking.isGatheringGas())){
        	scvWorking = null;
        }

        // Proceso para crear un vce.
        // Lo ideal son 3 recolectores por nodo de recursos.
        // Dado que todos los mapas empiezan con los mismos
        // nodos de minerales, se producen VCEs hasta que se llegue al límite.
        for (Unit unit : this.bwapi.getMyUnits()) {
            // Se comprueba si el número de minerales es superios a 50 unidades
            // y si no se han creado el número máximo de VCE.
        	//Se recupera el centro de mando y se le pone a fabricar un VCE.
            if (this.bwapi.getSelf().getMinerals() >= 50 && !scvReady
        		&& unit.getType() == UnitTypes.Terran_Command_Center) {                            
        			//Si el centro de mando está completado y no está entrenando unidades
        			if (unit.isCompleted() && !unit.isTraining()){
        				//Se añade un VCE a la cola
        				if (trainUnit(unit.getID(), UnitTypes.Terran_SCV)) {
        					VCEs++;
        					if (VCEs == 24){
        						scvReady = true;                		
        					}        					
        				}
        			}
    		}
        }

        // Proceso para la recolección de minerales
        for (Unit unit : this.bwapi.getMyUnits()) {
            // Se comprueba para cada unidad del jugador que está siendo 
            // controlado si este de tipo VCE
            if (unit.getType() == UnitTypes.Terran_SCV) {
                // Se comprueba si la unidad no está realizando ninguna tarea (isIdle)
                if ((unit.isIdle() || (unit.isIdle() && unit.isCarryingMinerals()) && !unit.equals(scvWorking))) {
                    // Se comprueban para todas las unidades de tipo neutral, aquella
                    // que no pertenencen a ningun jugador. 
                    for (Unit recurso : this.bwapi.getNeutralUnits()) {
                        // Se comprueba si la unidad es un deposito de minerales y si es
                        // no ha sido seleccionada previamente.                                 
                        if (recurso.getType().isMineralField()) {                                    
                            // Se calcula la distancia entre la unidad y el deposito de minerales
                            double distance = unit.getDistance(recurso);                                    
                            // Se comprueba si la distancia entre la unidad 
                            // y el deposito de minerales es menor a 300.
                            if (distance < 300) {
                                // Se ejecuta el comando para enviar a la unidad a recolertar
                                // minerales del deposito seleccionado.
                                unit.rightClick(recurso, false);
                                // Se añade el deposito a la lista de depositos en uso.
                                //this.claimedMinerals.add(minerals);                                	
                                break;
                            }
                        }
                    }
                    
                }
            }
        }
        
        // Proceso de construcción de depósitos de suministros.
        // Similar al proceso de construir barracones.
        // Se comprueba si el número de unidades generadas (getSupplyUsed) es mayor que el
        // número de unidades disponibles (getSupplyTotal) y si el número de unidades 
        // disponibles es mayor que el valor de la variable supplyCap. 
        if (bwapi.getSelf().getSupplyUsed() + 2 >= bwapi.getSelf().getSupplyTotal()
        		&& bwapi.getSelf().getSupplyTotal() > supplyCap) {
        	// Se comprueba si el número de minerales disponibles es 
        	// mayor o igual a 100.
        	if (bwapi.getSelf().getMinerals() >= 100) {
        		for (Unit vce : bwapi.getMyUnits()) {
        			// Se comprueba si la unidades es de tipo VCE.
        			if (vce.getType() == UnitTypes.Terran_SCV && (vce.isIdle() || vce.isGatheringMinerals()) && scvWorking == null ) {
        				Position pos = commandCenter.getPosition();
        				//Se construye un depósito de suministros
        				scvWorking = vce;
        				buildUnit(vce.getID(), UnitTypes.Terran_Supply_Depot, pos);
        				// Se asigna un nuevo valor a la variable supplyCap
        				supplyCap = bwapi.getSelf().getSupplyTotal();
        			}
        		}
        	}
        }

        // Proceso de contrucción de unos barracones (Para entrenar marines)
        // Se comprueba si el número de minerales disponibles es superior a 150
        // y no se ha construido otros barracones.
        if (this.bwapi.getSelf().getMinerals() >= 150 && barracones==0){
            //Construcción de los barracones cerca del Centro de Mando.
            //Hay que localizar el centro de mando y encontrar una posición cercana
            //Que permita construir unos barracones + 1 bloque extra para que las unidades puedan moverse.
            for (Unit vce : this.bwapi.getMyUnits()) {
            	// Se encuentra un VCE para construir
            	if (vce.getType() == UnitTypes.Terran_SCV && scvWorking==null) {
            		Position pos = commandCenter.getPosition();
    				scvWorking = vce;
    				// Se construye el barracón en la posición obtenida
    				buildUnit(vce.getID(), UnitTypes.Terran_Barracks, pos);
            	}
            }
        }

        // Proceso de generación de unidades, es necesario disponer de suministros
        // Se comprueba si el número de minerales disponibles es 
        // mayor o igual a 50.
        else{
        	//Generar soldados
        	if (bwapi.getSelf().getMinerals() >= 50) {
        		for (Unit unit : bwapi.getMyUnits()) {
        			// Se compruba si existe un Barracón y si está ha sido completada
        			if (unit.getType() == UnitTypes.Terran_Barracks && unit.isCompleted() && 
        					unit.getTrainingQueueSize() <= 2) {
        				trainUnit(unit.getID(), UnitTypes.Terran_Marine);
        			}
        		}
        	}        	
        }

//        // Proceso de movimiento y ataque
//        for (Unit unit : bwapi.getMyUnits()) {
//            // Se comprubea si la unidad es de zergling y si la unidad no tiene ninguna 
//            // tarea asignaga (isIdle).
//            if (unit.getType() == UnitTypes.Terran_Marine && unit.isIdle()) {
//                for (Unit enemy : bwapi.getEnemyUnits()) {
//                    // Se selecciona la posición de una unidad enemiga y 
//                    // se envia a la unidad seleccionada previamente a atacar. 
//                    unit.attack(enemy.getPosition(), false);
//                    break;
//                }
//            }
//        }
        
        //Construcción de la refinería.
        //Deben construirse barracones previamente, porque de cara al futuro
        //dado que para producir soldados no se necesita vespeno, es prioritario
        //sacar los barracones antes que hacer la refinería.
        if (barracones != 0 && 
        	scvWorking==null && this.bwapi.getSelf().getMinerals()>=100){
        	//se busca 1 constructor
        	for (Unit vce : this.bwapi.getMyUnits()){
        		//Se verifica que no haga nada con su vida
        		if (vce.isIdle() || vce.isGatheringMinerals()) {
        			//Se busca el vespeno
        			for (Unit vespeno : this.bwapi.getNeutralUnits()){
        				if (vespeno.getType() == UnitTypes.Resource_Vespene_Geyser){
        					//Se obtiene la pos. del vespeno.
        					Position pos = vespeno.getTilePosition();
        					scvWorking = vce;
        					buildUnit(vce.getID(), UnitTypes.Terran_Refinery, pos);
        					//Refinerias=1 -> Ya se ha construido la refinería
        					break;        					
        				}
        			}        
        			break;
        		}
        		
        	}
        }
    }

    @Override
    public void keyPressed(int keyCode) {}
    @Override
    public void matchEnd(boolean winner) {}	
    @Override
    public void sendText(String text) {}
    @Override
    public void receiveText(String text) {}
    @Override
    public void nukeDetect(Position p) {}
    @Override
    public void nukeDetect() {}
    @Override
    public void playerLeft(int playerID) {}
    @Override
    public void unitCreate(int unitID) {}
    @Override
    public void unitDestroy(int unitID) {}
    @Override
    public void unitDiscover(int unitID) {}
    @Override
    public void unitEvade(int unitID) {}
    @Override
    public void unitHide(int unitID) {}
    @Override
    public void unitMorph(int unitID) {}
    @Override
    public void unitShow(int unitID) {}
    @Override
    public void unitRenegade(int unitID) {}
    @Override
    public void saveGame(String gameName) {}
    @Override
    public void unitComplete(int unitID) { }
    @Override
    public void playerDropped(int playerID) {}
    
    /*
     * Método para entrenar una unidad en un edificio.
     * Si la unidad no se puede entrenar, devuelve False.
     * En caso contrario, True.
     */
    public boolean trainUnit(int buildingId, UnitType unitId){
    	if (bwapi.getSelf().getSupplyUsed() + unitId.getSupplyRequired() <= bwapi.getSelf().getSupplyTotal()){
    		//Se recupera el edificio
    		Unit building = bwapi.getUnit(buildingId);
    		building.train(unitId);
    		return true;
    	}
    	return false;
    }
    
    /*
     * Dada una posición, devuelve una posición
     * válida para construir un edificio.
     */
    public Position getValidPos(Position pos, UnitType building){
    	//Verifica si se puede construir en la posición dada y si la unidad asignada puede construir.
    	//En caso afirmativo devuelve True, en caso contrario False.
    	byte dirX = 1, dirY = 1;
    	if (Math.random() < 0.5){
    		dirX = -1;
    	}
    	if (Math.random() > 0.5){
    		dirY = -1;
    	}
    	while (!this.bwapi.canBuildHere(pos, building, false)){
    		Position s = new Position(dirX*building.getTileWidth(), dirY*building.getTileHeight());
    		pos = pos.translated(s);
    	}
    	return pos;
    }
    
    /*
     * Método para construir un edificio.
     * Para poder construir un edificio en X Pos, el VCE debe
     * moverse a esa posición antes de empezar a construir
     */
    public void buildUnit(int workerId, UnitType buildingId, Position pos){
    	if(buildingId.getMineralPrice() <= bwapi.getSelf().getMinerals() &&
    			buildingId.getGasPrice() <= bwapi.getSelf().getGas()) {
    		//Si no es refinería (porque la posición es fija)
    		//Se busca una posición de construcción válida
    		if (buildingId != UnitTypes.Terran_Refinery){
    			pos = this.getValidPos(pos, buildingId);    		
    		}

    		//Se recupera el trabajador
    		Unit worker = bwapi.getUnit(workerId);
    		
    		worker.build(pos, buildingId);
    	}
    }
}
